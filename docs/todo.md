# TODO

## Pre-Continue

- [x] Implement Anyhow support to the existing CallableError custom error enum.
- [x] Bump edition of all crates to 2021.
- [x] Broaden all dependency version to minor instead of patch.
- [x] Bump all dependencies to latest version.
- [x] Rework HTTP calls to use lib/http.
- [x] Replace custom errors with anyhow.
- [ ] Rename project references from Unicorn to Yuni.

