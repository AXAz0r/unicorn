use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct ResourceOrigins {
    pub users: HashMap<String, i64>,
    pub channels: HashMap<String, i64>,
    pub guilds: HashMap<String, i64>,
    pub functions: HashMap<String, i64>,
}

impl ResourceOrigins {
    fn fetch_generic(hm: &HashMap<String, i64>, key: &str) -> i64 {
        match hm.get(key) {
            Some(val) => val.to_owned(),
            None => 0,
        }
    }

    fn update_generic(hm: &mut HashMap<String, i64>, key: &str, val: i64) {
        if hm.contains_key(key) {
            let _ = hm.remove(key);
        }
        hm.insert(key.to_owned(), val);
    }

    pub fn get_user(&self, uid: u64) -> i64 {
        Self::fetch_generic(&self.users, &uid.to_string())
    }

    pub fn get_channel(&self, cid: u64) -> i64 {
        Self::fetch_generic(&self.channels, &cid.to_string())
    }

    pub fn get_guild(&self, gid: u64) -> i64 {
        Self::fetch_generic(&self.guilds, &gid.to_string())
    }

    pub fn get_function(&self, fid: &str) -> i64 {
        Self::fetch_generic(&self.functions, fid)
    }

    pub fn add_user(&mut self, uid: u64, amount: i64) {
        let current = self.get_user(uid);
        let value = current + amount;
        Self::update_generic(&mut self.users, &uid.to_string(), value);
    }

    pub fn add_channel(&mut self, cid: u64, amount: i64) {
        let current = self.get_channel(cid);
        let value = current + amount;
        Self::update_generic(&mut self.channels, &cid.to_string(), value);
    }

    pub fn add_guild(&mut self, gid: u64, amount: i64) {
        let current = self.get_guild(gid);
        let value = current + amount;
        Self::update_generic(&mut self.guilds, &gid.to_string(), value);
    }

    pub fn add_function(&mut self, fid: String, amount: i64) {
        let current = self.get_function(&fid);
        let value = current + amount;
        Self::update_generic(&mut self.functions, &fid, value);
    }
}
