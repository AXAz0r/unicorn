use bson::Bson;
use log::error;
use serde::{Deserialize, Serialize};

use crate::change::ResourceChange;
use crate::origins::ResourceOrigins;

pub mod change;
pub mod origin;
pub mod origins;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct UnicornResource {
    pub user_id: i64,
    pub current: i64,
    pub total: i64,
    pub ranked: i64,
    pub origins: ResourceOrigins,
    pub expenses: ResourceOrigins,
}

impl UnicornResource {
    pub fn add<F>(&mut self, f: F) -> &mut Self
    where
        F: FnOnce(&mut ResourceChange) -> &mut ResourceChange,
    {
        let mut rsc = ResourceChange::new(String::new());
        f(&mut rsc);
        if rsc.amount > 0 {
            self.current += rsc.amount;
            if rsc.tracked {
                self.total += rsc.amount;
                if rsc.ranked {
                    self.ranked += rsc.amount;
                    if let Some(uid) = rsc.user {
                        self.origins.add_user(uid, rsc.amount);
                    }
                    if let Some(cid) = rsc.channel {
                        self.origins.add_channel(cid, rsc.amount);
                    }
                    if let Some(gid) = rsc.guild {
                        self.origins.add_guild(gid, rsc.amount);
                    }
                }
            }
        }
        self
    }

    pub fn del<F>(&mut self, f: F) -> &mut Self
    where
        F: FnOnce(&mut ResourceChange) -> &mut ResourceChange,
    {
        let mut rsc = ResourceChange::new(String::new());
        f(&mut rsc);
        if rsc.amount > 0 {
            self.current -= rsc.amount;
            if rsc.tracked {
                if let Some(uid) = rsc.user {
                    self.expenses.add_user(uid, rsc.amount);
                }
                if let Some(cid) = rsc.channel {
                    self.expenses.add_channel(cid, rsc.amount);
                }
                if let Some(gid) = rsc.guild {
                    self.expenses.add_guild(gid, rsc.amount);
                }
            }
        }
        self
    }

    pub fn to_bson(&self) -> Option<Bson> {
        match bson::to_bson(self) {
            Ok(bs) => Some(bs),
            Err(why) => {
                error!("Failed encoding resource: {}", why);
                None
            }
        }
    }

    fn get_indexes(level: usize) -> [usize; 2] {
        let slevel = level.to_string();
        let chars = slevel.split("").collect::<Vec<&str>>();
        let suffix = chars
            .get(slevel.len() - 1)
            .unwrap_or(&"0")
            .parse::<usize>()
            .unwrap_or(0);
        let prefix = if slevel.len() >= 2 {
            chars
                .get(slevel.len() - 2)
                .unwrap_or(&"0")
                .parse::<usize>()
                .unwrap_or(0)
        } else {
            0
        };
        [prefix, suffix]
    }

    fn get_level(val: i64, leveler: f64) -> i64 {
        (if val >= 0 { val as f64 / leveler } else { 0f64 }) as i64
    }

    pub fn get_title(val: i64, leveler: f64, prefixes: &[&str], suffixes: &[&str]) -> String {
        let level = Self::get_level(val, leveler);
        let indexes = Self::get_indexes(level as usize);
        let (pix, six) = (indexes[0], indexes[1]);
        format!(
            "{} {}",
            prefixes.get(pix).unwrap_or(&"Unknown"),
            suffixes.get(six).unwrap_or(&"Ghost")
        )
    }
}
