use serenity::model::user::User;

pub struct Avatar;

impl Avatar {
    pub fn url(user: User) -> String {
        match user.avatar_url() {
            Some(out) => out,
            None => user.default_avatar_url(),
        }
    }
}
