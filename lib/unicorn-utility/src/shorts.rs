use serenity::client::Context;
use serenity::model::channel::Message;
use serenity::model::guild::Role;
use serenity::model::Timestamp;

pub struct UnicornShorts;
pub const DATE_FMT: &str = "%a, %d. %h. %Y";

impl UnicornShorts {
    pub async fn fetch_roles(ctx: &Context, msg: &Message) -> Vec<Role> {
        let mut roles = Vec::<Role>::new();
        let guild = msg.guild(&ctx.cache).map(|gref| gref.clone());
        if let Some(guild) = guild {
            for (_, role) in guild.roles.clone() {
                if let Ok(res) = msg.author.has_role(&ctx, guild.id, &role).await {
                    if res {
                        roles.push(role);
                    }
                }
            }
        };
        roles
    }

    pub fn format_timestamp(ts: Timestamp) -> String {
        let ts = ts.unix_timestamp();
        if let Some(dt) = chrono::DateTime::from_timestamp(ts, 0) {
            dt.format(DATE_FMT).to_string()
        } else {
            "Unknown".to_owned()
        }
    }
}
