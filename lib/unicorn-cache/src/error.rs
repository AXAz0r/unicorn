pub enum UnicornCacheError {
    RedisError(()),
}

impl From<redis::RedisError> for UnicornCacheError {
    fn from(_err: redis::RedisError) -> Self {
        Self::RedisError(())
    }
}
