use serenity::client::Context;
use serenity::model::channel::{Message, Reaction};
use std::collections::HashMap;

use unicorn_cache::handler::CacheHandler;
use unicorn_config::config::Configuration;
use unicorn_cooldown::UnicornCooldown;
use unicorn_database::settings::GuildSettings;
use unicorn_database::DatabaseHandler;
use unicorn_information::ModuleInformation;

use crate::argument::CommandArguments;

#[derive(Clone)]
pub struct CommandPayload {
    pub db: DatabaseHandler,
    pub meta: HashMap<String, ModuleInformation>,
    pub cfg: Configuration,
    pub cd: UnicornCooldown,
    pub args: CommandArguments,
    pub cache: CacheHandler,
    pub ctx: Context,
    pub msg: Message,
    pub settings: Option<GuildSettings>,
}

impl CommandPayload {
    pub async fn new(
        ctx: &Context,
        meta: &HashMap<String, ModuleInformation>,
        cfg: &Configuration,
        db: &DatabaseHandler,
        cd: &UnicornCooldown,
        cache: &CacheHandler,
        msg: &Message,
    ) -> Self {
        let settings = if let Some(gid) = msg.guild_id {
            Some(db.clone().get_settings(gid.get()).await)
        } else {
            None
        };
        Self {
            db: db.clone(),
            meta: meta.clone(),
            cfg: cfg.clone(),
            cd: cd.clone(),
            cache: cache.clone(),
            ctx: ctx.clone(),
            msg: msg.clone(),
            args: CommandArguments::new("", Vec::new()),
            settings,
        }
    }

    pub fn with_arguments(mut self, args: CommandArguments) -> CommandPayload {
        self.args = args;
        self
    }

    pub fn get_prefix(&self) -> String {
        match &self.settings {
            Some(settings) => match &settings.prefix {
                Some(pfx) => pfx.to_owned(),
                None => self.db.cfg.preferences.prefix.clone(),
            },
            None => self.db.cfg.preferences.prefix.clone(),
        }
    }
}

#[derive(Clone)]
pub struct ReactionPayload {
    pub db: DatabaseHandler,
    pub meta: HashMap<String, ModuleInformation>,
    pub cfg: Configuration,
    pub cd: UnicornCooldown,
    pub cache: CacheHandler,
    pub ctx: Context,
    pub reaction: Reaction,
}

impl ReactionPayload {
    pub fn new(
        ctx: &Context,
        meta: &HashMap<String, ModuleInformation>,
        cfg: &Configuration,
        db: &DatabaseHandler,
        cd: &UnicornCooldown,
        cache: &CacheHandler,
        reaction: &Reaction,
    ) -> Self {
        Self {
            db: db.clone(),
            meta: meta.clone(),
            cfg: cfg.clone(),
            cd: cd.clone(),
            cache: cache.clone(),
            ctx: ctx.clone(),
            reaction: reaction.clone(),
        }
    }
}
