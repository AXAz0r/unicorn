use serenity::builder::{CreateEmbed, CreateMessage};
use std::any::Any;
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;
use unicorn_information::CommandInformation;

use crate::argument::CommandArgument;
use crate::payload::CommandPayload;
use crate::perms::common::{PermissionParams, UnicornPermissions};
use serenity::http::CacheHttp;

const COMMAND_COOLDOWN: i32 = 1;
const PERMISSION_EMOTE: char = '🔒';
const NO_WRITE_EMOTE: char = '😶';
const COOLDOWN_EMOTE: char = '❄';
const BURST_EMOTE: char = '🕥';
const DISCORD_PERMISSION_EMOTE: &str = "🚧";
const DISCORD_PERMISSION_COLOR: u64 = 0xff_cc_4d;
const NO_EMBED_ERROR: &str =
    "**Error**: My functions require the \"Embed Links\" permission which seems to be missing.";

#[serenity::async_trait]
pub trait UnicornCommand: Any + Send + Sync {
    /*
    Command Detail Functions
    */
    fn command_name(&self) -> &str;
    fn category(&self) -> &str;
    fn nsfw(&self) -> bool {
        false
    }
    fn owner(&self) -> bool {
        false
    }
    fn premium(&self) -> bool {
        false
    }
    fn allow_dm(&self) -> bool {
        false
    }
    fn aliases(&self) -> Vec<&str> {
        Vec::<&str>::new()
    }
    fn example(&self) -> &str {
        ""
    }
    fn description(&self) -> &str;
    fn information(&self) -> CommandInformation {
        let mut aliases = Vec::<String>::new();
        for alstr in self.aliases() {
            aliases.push(alstr.to_owned());
        }
        CommandInformation {
            name: self.command_name().to_owned(),
            category: self.category().to_owned(),
            nsfw: self.nsfw(),
            owner: self.owner(),
            dm: self.allow_dm(),
            aliases,
            example: self.example().to_owned(),
            description: self.description().to_owned(),
        }
    }

    /*
    Various Pre-Execution Checks
    */
    async fn permissions(&self, pld: &CommandPayload) -> UnicornPermissions {
        let params = PermissionParams::new(
            self.command_name().to_owned(),
            self.category().to_owned(),
            self.allow_dm(),
            self.nsfw(),
            self.owner(),
            pld.clone(),
        );
        let mut perms = UnicornPermissions::new(params);
        perms.check().await;
        perms
    }

    async fn basic_permissions(&self, pld: &CommandPayload) -> bool {
        let me = pld.ctx.cache.current_user().clone();
        let guild = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
        let channel = if let Ok(chn) = pld.msg.channel(&pld.ctx).await {
            chn.guild()
        } else {
            None
        };
        match guild {
            Some(gld) => {
                let gld = gld.clone();
                if let Some(chn) = channel {
                    if let Ok(me) = gld.member(&pld.ctx.http(), &me.id).await {
                        let perms = gld.user_permissions_in(&chn, &me);
                        if perms.send_messages() {
                            if perms.embed_links() {
                                true
                            } else {
                                let message = CreateMessage::default().content(NO_EMBED_ERROR);
                                let _ = pld
                                    .msg
                                    .channel_id
                                    .send_message(&pld.ctx.http, message)
                                    .await;
                                false
                            }
                        } else {
                            if perms.add_reactions() {
                                let _ = pld.msg.react(&pld.ctx.http, NO_WRITE_EMOTE).await;
                            }
                            false
                        }
                    } else {
                        false
                    }
                } else {
                    false
                }
            }
            None => true,
        }
    }

    async fn discord_permissions(&self, _pld: &mut CommandPayload) -> bool {
        true
    }

    async fn cooling(&self, pld: &mut CommandPayload) -> bool {
        let is_owner = pld.cfg.discord.owners.contains(&pld.msg.author.id.get());
        if !is_owner {
            let key = format!("cmd:{}:{}", self.command_name(), pld.msg.author.id);
            let cdi = pld.cd.fetch_cooldown(key.clone()).await;
            if !cdi.active {
                pld.cd
                    .set_cooldown(key, pld.msg.author.id.get(), COMMAND_COOLDOWN)
                    .await;
                false
            } else {
                let _ = pld.msg.react(pld.ctx.http.clone(), BURST_EMOTE).await;
                true
            }
        } else {
            false
        }
    }

    /*
    Command Execution Calls
    */
    async fn on_load(&self, _db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {}
    fn pre_run(&self) {}
    fn parameters(&self) -> Vec<CommandArgument> {
        Vec::new()
    }
    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()>;
    async fn run(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        if self.basic_permissions(pld).await {
            if self.discord_permissions(pld).await {
                let perms = self.permissions(pld).await;
                if perms.ok() {
                    if !self.cooling(pld).await {
                        self.execute(pld).await?
                    } else {
                        let _ = pld.msg.react(&pld.ctx.http, COOLDOWN_EMOTE).await;
                    }
                } else {
                    match perms.global.response() {
                        Some(resp) => {
                            let embed = CreateEmbed::default()
                                .color(resp.color)
                                .title(format!("{} {}", resp.icon, resp.title))
                                .description(resp.description);
                            let message = CreateMessage::default().embed(embed);
                            let res = pld
                                .msg
                                .channel_id
                                .send_message(&pld.ctx.http, message)
                                .await;
                            if res.is_err() {
                                let _ = pld.msg.react(&pld.ctx.http, resp.icon).await;
                            }
                        }
                        None => {
                            let _ = pld.msg.react(&pld.ctx.http, PERMISSION_EMOTE).await;
                        }
                    }
                }
            } else {
                let title = format!("{} Missing Discord permissions.", DISCORD_PERMISSION_EMOTE);
                let desc = format!(
                    "{} **{}**. {} `{}help {}` {}",
                    "You seem to be missing the required permissions to execute",
                    self.command_name(),
                    "Please run",
                    pld.get_prefix(),
                    self.command_name(),
                    "for more information on what permissions are required."
                );
                let embed = CreateEmbed::default()
                    .color(DISCORD_PERMISSION_COLOR)
                    .title(title)
                    .description(desc);
                let message = CreateMessage::default().embed(embed);
                let _ = pld
                    .msg
                    .channel_id
                    .send_message(&pld.ctx.http, message)
                    .await;
            }
        }
        Ok(())
    }
    fn post_run(&self) {}
}
