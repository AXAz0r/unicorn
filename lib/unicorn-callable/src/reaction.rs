use std::any::Any;

use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;

use crate::payload::ReactionPayload;

#[serenity::async_trait]
pub trait UnicornReaction: Any + Send + Sync {
    async fn execute(&self, pld: &mut ReactionPayload) -> anyhow::Result<()>;
    fn emojis(&self) -> Option<Vec<&str>>;
    async fn on_load(&self, _db: &DatabaseHandler, _cfg: &mut ConfigurationBuilder) {}
    async fn run(&self, pld: &mut ReactionPayload) -> anyhow::Result<()> {
        self.execute(pld).await
    }
}
