use crate::argument::CommandArgument;
use crate::payload::CommandPayload;

pub struct Paginator<T> {
    source: Vec<T>,
    pub page: usize,
    pub page_size: usize,
    pub page_count: usize,
    pub start_range: usize,
    pub end_range: usize,
}

impl Paginator<()> {
    pub fn pagination_args() -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("page", true).available_when(|a| a.parse::<u16>().is_ok()),
            CommandArgument::new("pagesize", true).with_value(),
        ]
    }
}

impl<T> Paginator<T> {
    pub fn new(pld: &CommandPayload, source: Vec<T>) -> Self {
        let mut page = if pld.args.has_argument("page") {
            pld.args.get("page").parse::<usize>().unwrap_or(1)
        } else {
            1
        };
        let page_size = if pld.args.has_argument("pagesize") {
            pld.args.get("pagesize").parse::<usize>().unwrap_or(10)
        } else {
            10
        };
        let pages = source.len() / page_size;
        let count = source.len();
        let page_count = if (count % page_size == 0) && (count != 0) {
            pages
        } else {
            pages + 1
        };
        page = if page > page_count {
            page_count
        } else if page > 0 {
            page
        } else {
            1
        };
        let start_range = (page - 1) * page_size;
        let mut end_range = page * page_size;
        if end_range > count {
            end_range = count
        }
        Self {
            page,
            page_size,
            source,
            start_range,
            end_range,
            page_count,
        }
    }

    pub fn fetch_page(self: &Paginator<T>) -> &[T] {
        &self.source[self.start_range..self.end_range]
    }
}
