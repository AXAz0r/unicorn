#[cfg(test)]
mod tests {
    use crate::argument::{CommandArgument, CommandArguments};

    #[test]
    fn test_no_argument() {
        let arg = vec![
            CommandArgument::new("test", false),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command", arg);
        assert_eq!(args.satisfied(), false);
    }

    #[test]
    fn test_indexed_argument() {
        let arg = vec![
            CommandArgument::new("test", false),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command a b", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.get("test"), "a");
        assert_eq!(args.has_argument("test2"), true);
        assert_eq!(args.get("test2"), "b");
    }

    #[test]
    fn test_flag_argument_first() {
        let arg = vec![
            CommandArgument::new("test", false).flag(),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command test b", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.has_argument("test2"), true);
        assert_eq!(args.get("test2"), "b");
    }

    #[test]
    fn test_flag_argument_second() {
        let arg = vec![
            CommandArgument::new("test", false).flag(),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command b test", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.has_argument("test2"), true);
        assert_eq!(args.get("test2"), "b");
    }

    #[test]
    fn test_allows_spaces() {
        let arg = vec![
            CommandArgument::new("test", false).allows_spaces(),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command b test", arg);
        assert_eq!(args.satisfied(), false);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.get("test"), "b test");
        assert_eq!(args.has_argument("test2"), false);
    }

    #[test]
    fn test_with_value() {
        let arg = vec![
            CommandArgument::new("test", false).with_value(),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command test b", arg);
        assert_eq!(args.satisfied(), false);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.get("test"), "b");
        assert_eq!(args.has_argument("test2"), false);
    }

    #[test]
    fn test_with_value_and_others() {
        let arg = vec![
            CommandArgument::new("test", false).with_value(),
            CommandArgument::new("test2", false),
        ];
        let args = CommandArguments::new("command test a b", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("test"), true);
        assert_eq!(args.get("test"), "a");
        assert_eq!(args.has_argument("test2"), true);
        assert_eq!(args.get("test2"), "b");
    }

    #[test]
    fn test_with_group() {
        let arg = vec![
            CommandArgument::new("first", false),
            CommandArgument::new("letter", true)
                .group("test")
                .with_value(),
            CommandArgument::new("number", true)
                .group("test")
                .with_value(),
            CommandArgument::new("colour", true)
                .group("test")
                .with_value(),
            CommandArgument::new("last", false),
        ];
        let args = CommandArguments::new("command a letter z b", arg.clone());
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("first"), true);
        assert_eq!(args.get("first"), "a");
        assert_eq!(args.has_argument("last"), true);
        assert_eq!(args.get("last"), "b");
        assert_eq!(args.has_argument("letter"), true);
        if let Some(g1) = args.get_group("test") {
            assert_eq!(g1.0, "letter");
            assert_eq!(g1.1, "z");
        }
        let args2 = CommandArguments::new("command a b number 1", arg.clone());
        assert_eq!(args2.satisfied(), true);
        assert_eq!(args2.has_argument("first"), true);
        assert_eq!(args2.get("first"), "a");
        assert_eq!(args2.has_argument("last"), true);
        assert_eq!(args2.get("last"), "b");
        assert_eq!(args2.has_argument("number"), true);
        if let Some(g1) = args2.get_group("test") {
            assert_eq!(g1.0, "number");
            assert_eq!(g1.1, "1");
        }
        let args3 = CommandArguments::new("command a colour red b", arg.clone());
        assert_eq!(args3.satisfied(), true);
        assert_eq!(args3.has_argument("first"), true);
        assert_eq!(args3.get("first"), "a");
        assert_eq!(args3.has_argument("last"), true);
        assert_eq!(args3.get("last"), "b");
        assert_eq!(args3.has_argument("colour"), true);
        if let Some(g1) = args3.get_group("test") {
            assert_eq!(g1.0, "colour");
            assert_eq!(g1.1, "red");
        }
    }

    #[test]
    fn test_with_group_missing() {
        let arg = vec![
            CommandArgument::new("first", false),
            CommandArgument::new("letter", true)
                .group("test")
                .with_value(),
            CommandArgument::new("number", true)
                .group("test")
                .with_value(),
            CommandArgument::new("colour", true)
                .group("test")
                .with_value(),
            CommandArgument::new("last", false),
        ];
        let args = CommandArguments::new("command a b", arg);
        assert_eq!(args.satisfied(), false);
        assert_eq!(args.has_argument("first"), true);
        assert_eq!(args.get("first"), "a");
        assert_eq!(args.has_argument("last"), true);
        assert_eq!(args.get("last"), "b");
        assert_eq!(args.has_argument("test"), false);
        assert_eq!(args.get_group("test"), None);
    }

    #[test]
    fn test_mulitple_with_value() {
        let arg = vec![
            CommandArgument::new("first", false),
            CommandArgument::new("letter", true).with_value(),
            CommandArgument::new("number", true).with_value(),
        ];
        let args = CommandArguments::new("command a letter x number 3", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("first"), true);
        assert_eq!(args.get("first"), "a");
        assert_eq!(args.has_argument("letter"), true);
        assert_eq!(args.get("letter"), "x");
        assert_eq!(args.has_argument("number"), true);
        assert_eq!(args.get("number"), "3");
    }

    #[test]
    fn test_with_available_when() {
        let arg = vec![
            CommandArgument::new("always first", false),
            CommandArgument::new("only if number", true)
                .available_when(|f| f.parse::<usize>().is_ok()),
            CommandArgument::new("last", false),
        ];
        let args1 = CommandArguments::new("command a b", arg.clone());
        assert_eq!(args1.satisfied(), true);
        assert_eq!(args1.has_argument("always first"), true);
        assert_eq!(args1.get("always first"), "a");
        assert_eq!(args1.has_argument("only if number"), false);
        assert_eq!(args1.has_argument("last"), true);
        assert_eq!(args1.get("last"), "b");
        let args2 = CommandArguments::new("command a 1 b", arg.clone());
        assert_eq!(args2.satisfied(), true);
        assert_eq!(args2.has_argument("always first"), true);
        assert_eq!(args2.get("always first"), "a");
        assert_eq!(args2.has_argument("only if number"), true);
        assert_eq!(args2.get_or_default("only if number", 2), 1);
        assert_eq!(args2.has_argument("last"), true);
        assert_eq!(args2.get("last"), "b");
        let args3 = CommandArguments::new("command a b 1", arg.clone());
        assert_eq!(args3.satisfied(), true);
        assert_eq!(args3.has_argument("always first"), true);
        assert_eq!(args3.get("always first"), "a");
        assert_eq!(args3.has_argument("only if number"), true);
        assert_eq!(args3.get_or_default("only if number", 2), 1);
        assert_eq!(args3.has_argument("last"), true);
        assert_eq!(args3.get("last"), "b");
        let args4 = CommandArguments::new("command 1 a b", arg.clone());
        assert_eq!(args4.satisfied(), true);
        assert_eq!(args4.has_argument("always first"), true);
        assert_eq!(args4.get("always first"), "1");
        assert_eq!(args4.has_argument("only if number"), false);
        assert_eq!(args4.has_argument("last"), true);
        assert_eq!(args4.get("last"), "a");
    }

    #[test]
    fn test_with_available_when_anywhere() {
        let arg = vec![
            CommandArgument::new("first", false),
            CommandArgument::new("first number anywhere", true)
                .available_when(|f| f.parse::<usize>().is_ok())
                .anywhere(),
            CommandArgument::new("last", false),
        ];
        let args1 = CommandArguments::new("command 1 first last", arg.clone());
        assert_eq!(args1.satisfied(), true);
        assert_eq!(args1.has_argument("first"), true);
        assert_eq!(args1.get("first"), "first");
        assert_eq!(args1.has_argument("first number anywhere"), true);
        assert_eq!(args1.get_or_default("first number anywhere", 0), 1);
        assert_eq!(args1.has_argument("last"), true);
        assert_eq!(args1.get("last"), "last");
        let args2 = CommandArguments::new("command first 1 last", arg.clone());
        assert_eq!(args2.satisfied(), true);
        assert_eq!(args2.has_argument("first"), true);
        assert_eq!(args2.get("first"), "first");
        assert_eq!(args2.has_argument("first number anywhere"), true);
        assert_eq!(args2.get_or_default("first number anywhere", 0), 1);
        assert_eq!(args2.has_argument("last"), true);
        assert_eq!(args2.get("last"), "last");
        let args3 = CommandArguments::new("command first last 1", arg.clone());
        assert_eq!(args3.satisfied(), true);
        assert_eq!(args3.has_argument("first"), true);
        assert_eq!(args3.get("first"), "first");
        assert_eq!(args3.has_argument("first number anywhere"), true);
        assert_eq!(args3.get_or_default("first number anywhere", 0), 1);
        assert_eq!(args3.has_argument("last"), true);
        assert_eq!(args3.get("last"), "last");
        let args4 = CommandArguments::new("command first last", arg.clone());
        assert_eq!(args4.satisfied(), true);
        assert_eq!(args4.has_argument("first"), true);
        assert_eq!(args4.get("first"), "first");
        assert_eq!(args4.has_argument("first number anywhere"), false);
        assert_eq!(args4.get_or_default("first number anywhere", 0), 0);
        assert_eq!(args4.has_argument("last"), true);
        assert_eq!(args4.get("last"), "last");
    }

    #[test]
    fn test_invite_text() {
        let arg = vec![CommandArgument::new("text", true).flag()];
        let args = CommandArguments::new("--invite text", arg);
        assert_eq!(args.satisfied(), true);
        assert_eq!(args.has_argument("text"), true);
    }
}
