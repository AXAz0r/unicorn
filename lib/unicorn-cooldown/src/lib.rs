use std::sync::Arc;

use bson::doc;
use chrono::Utc;
use log::{error, info};

use unicorn_cache::handler::CacheHandler;
use unicorn_database::DatabaseHandler;

use crate::entry::{CooldownDatabaseEntry, CooldownEntry};

mod entry;

#[derive(Clone)]
pub struct UnicornCooldown {
    pub db: Arc<DatabaseHandler>,
    pub cache: Arc<CacheHandler>,
}

impl UnicornCooldown {
    pub async fn new(db: Arc<DatabaseHandler>, cache: Arc<CacheHandler>) -> Self {
        let mut ucd = Self { db, cache };
        ucd.clean_expired().await;
        ucd
    }

    async fn clean_expired(&mut self) {
        if let Some(cli) = self.db.get_client() {
            let now = Utc::now().timestamp() as i32;
            let lookup = doc! {"end_stamp": {"$lt": now}};
            match cli
                .database(&self.db.cfg.database.name)
                .collection::<bson::Document>("cooldowns")
                .delete_many(lookup, None)
                .await
            {
                Ok(dres) => {
                    if dres.deleted_count > 0 {
                        info!("Deleted {} expired cooldown documents.", dres.deleted_count)
                    }
                }
                Err(_) => {
                    error!("Failed deleting expired cooldown documents.");
                }
            }
        }
    }

    async fn remove_from_cache(&mut self, key: String) {
        self.cache.del(key).await;
    }

    async fn fetch_from_cache(&mut self, key: String) -> Option<CooldownEntry> {
        let cache_key = format!("cooldown:{}", &key);
        match self.cache.get(cache_key.clone()).await {
            Some(val) => match val.parse::<i32>() {
                Ok(stamp) => {
                    let cd = CooldownEntry::new(key, stamp);
                    if !cd.active {
                        self.remove_from_cache(cache_key.clone()).await;
                    }
                    Some(cd)
                }
                Err(why) => {
                    error!("Failed parsing cached cooldown value for {}: {}", key, why);
                    None
                }
            },
            None => None,
        }
    }

    async fn get_from_db(&self, key: String) -> Option<CooldownEntry> {
        CooldownDatabaseEntry::get(&self.db, key)
            .await
            .map(CooldownEntry::from)
    }

    pub async fn fetch_cooldown(&mut self, key: String) -> CooldownEntry {
        match self.fetch_from_cache(key.clone()).await {
            Some(cd) => cd,
            None => match self.get_from_db(key.clone()).await {
                Some(cd) => cd,
                None => CooldownEntry::new(key, 0),
            },
        }
    }

    pub async fn del_cooldown(&mut self, key: String) {
        self.remove_from_cache(key.clone()).await;
        CooldownDatabaseEntry::remove(&self.db, key).await;
    }

    async fn set_in_cache(&mut self, key: String, stamp: i32) {
        self.cache
            .set(format!("cooldown:{}", key), stamp.to_string())
            .await;
    }

    async fn save_in_db(&self, key: String, stamp: i32) {
        let cdbe = CooldownDatabaseEntry::new(key, stamp);
        cdbe.save(&self.db).await;
    }

    pub async fn set_cooldown(&mut self, key: String, user: u64, time: i32) {
        let is_owner = self.db.cfg.discord.owners.contains(&user);
        if !is_owner {
            let stamp = Utc::now().timestamp() as i32 + time;
            self.set_in_cache(key.clone(), stamp).await;
            if time > 60 {
                // Don't bother storing the cooldown if it's short.
                self.save_in_db(key, stamp).await;
            }
        }
    }
}
