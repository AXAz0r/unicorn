use log::error;
use serenity::prelude::GatewayIntents;
use serenity::Client;

use unicorn_config::config::Configuration;

use crate::error::UnicornClientError;
use crate::handler::UnicornHandler;

pub struct UnicornClient {
    cfg: Configuration,
    client: Client,
}

impl UnicornClient {
    pub async fn new(mut cfg: Configuration) -> Result<Self, UnicornClientError> {
        let handler = UnicornHandler::new(&mut cfg).await?;
        let intents = GatewayIntents::all();
        let client = Client::builder(&cfg.discord.token, intents)
            .event_handler(handler)
            .await?;
        cfg.describe();
        Ok(Self { cfg, client })
    }

    pub async fn run(&mut self) -> Result<(), UnicornClientError> {
        if !&self.cfg.discord.token.is_empty() {
            self.client.start_autosharded().await?;
            Ok(())
        } else {
            let err = "The token is empty, shutting down.";
            error!("{}", err);
            panic!("{}", err);
        }
    }
}
