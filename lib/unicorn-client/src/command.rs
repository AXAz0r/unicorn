use serenity::model::channel::Message;

#[derive(Debug, Clone)]
pub struct CommandQuery {
    pub ok: bool,
    pub name: String
}

impl CommandQuery {
    pub fn new(pfx: &str, msg: &Message) -> Self {
        let ok: bool;
        let mut name = "".to_owned();
        if msg.content.starts_with(pfx) {
            ok = true;
            if msg.content.len() > pfx.len() {
                let prefixless = msg.content.split_at(pfx.len()).1;
                let pieces: Vec<&str> = prefixless.split(' ').collect();
                for (piece_index, piece) in pieces.iter().enumerate() {
                    if piece_index == 0 {
                        name = piece.to_owned().to_lowercase();
                    }
                }
            }
        } else {
            ok = false;
        }
        Self { ok, name }
    }
}
