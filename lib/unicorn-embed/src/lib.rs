use serenity::builder::{CreateEmbed, CreateMessage};

// OK
const OK_ICON: &str = "✅";
const OK_COLOR: u64 = 0x77_b2_55;

// Info
const INFO_ICON: &str = "ℹ";
const INFO_COLOR: u64 = 0x3b_88_c3;

// Warn
const WARN_ICON: &str = "⚠";
const WARN_COLOR: u64 = 0xff_cc_4d;

// Error
const ERROR_ICON: &str = "❗";
const ERROR_COLOR: u64 = 0xbe_19_31;

// Denied
const DENIED_ICON: &str = "⛔";
const DENIED_COLOR: u64 = 0xbe_19_31;

// Not Found
const NOT_FOUND_ICON: &str = "🔍";
const NOT_FOUND_COLOR: u64 = 0x69_69_69;

pub struct UnicornEmbed;

impl UnicornEmbed {
    pub fn small_embed(icon: &str, color: u64, text: impl ToString) -> CreateMessage {
        let mut msg = CreateMessage::default();
        let embed =
            CreateEmbed::default()
                .color(color)
                .title(format!("{} {}", icon, text.to_string()));
        msg = msg.embed(embed);
        msg
    }

    pub fn ok(text: impl ToString) -> CreateMessage {
        Self::small_embed(OK_ICON, OK_COLOR, text)
    }

    pub fn info(text: impl ToString) -> CreateMessage {
        Self::small_embed(INFO_ICON, INFO_COLOR, text)
    }

    pub fn warn(text: impl ToString) -> CreateMessage {
        Self::small_embed(WARN_ICON, WARN_COLOR, text)
    }

    pub fn error(text: impl ToString) -> CreateMessage {
        Self::small_embed(ERROR_ICON, ERROR_COLOR, text)
    }

    pub fn denied(text: impl ToString) -> CreateMessage {
        Self::small_embed(DENIED_ICON, DENIED_COLOR, text)
    }

    pub fn not_found(text: impl ToString) -> CreateMessage {
        Self::small_embed(NOT_FOUND_ICON, NOT_FOUND_COLOR, text)
    }
}
