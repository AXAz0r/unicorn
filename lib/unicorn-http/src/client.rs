use reqwest::Client;
use reqwest::Response;

pub struct UnicornHttpClient {
    pub client: Client,
}

impl UnicornHttpClient {
    pub fn new() -> anyhow::Result<Self> {
        let client = Client::builder().user_agent("unicorn/0.1.0").build()?;
        Ok(Self { client })
    }

    pub async fn get(&self, url: &str) -> anyhow::Result<Response> {
        Ok(self.client.get(url).send().await?)
    }
}
