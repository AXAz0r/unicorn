use std::collections::HashMap;
use std::error::Error;
use log::{debug, error, info};

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::module::UnicornModule;
use unicorn_callable::reaction::UnicornReaction;
use unicorn_config::builder::ConfigurationBuilder;
use unicorn_database::DatabaseHandler;
use unicorn_information::ModuleInformation;

#[derive(Default)]
pub struct ModuleManager {
    _modules: Vec<Box<dyn UnicornModule>>,
    aliases: HashMap<String, String>,
    commands: HashMap<String, Box<dyn UnicornCommand>>,
    reaction_map: HashMap<String, Vec<usize>>,
    reactions: HashMap<usize, Box<dyn UnicornReaction>>,
    pub information: HashMap<String, ModuleInformation>,
}

impl ModuleManager {
    pub fn new() -> Self {
        Self {
            _modules: Self::fetch_modules(),
            ..Self::default()
        }
    }

    fn fetch_modules() -> Vec<Box<dyn UnicornModule>> {
        vec![
            mdl_fun::FunModule::boxed(),
            mdl_help::HelpModule::boxed(),
            mdl_information::InformationModule::boxed(),
            mdl_interactions::InteractionsModule::boxed(),
            mdl_test::TestModule::boxed(),
            mdl_nsfw::NSFWModule::boxed(),
            mdl_owner::OwnerModule::boxed(),
            mdl_searches::SearchModule::boxed(),
            mdl_settings::SettingsModule::boxed(),
        ]
    }

    pub async fn load_modules(&mut self, db: &DatabaseHandler, cfg: &mut ConfigurationBuilder) {
        for module in Self::fetch_modules() {
            if let Err(why) = self.load_module(module, db, cfg).await {
                error!("Failed to load the module: {:?}", why);
            }
        }
    }

    pub async fn load_module(
        &mut self,
        mdl: Box<dyn UnicornModule>,
        db: &DatabaseHandler,
        cfg: &mut ConfigurationBuilder,
    ) -> Result<(), Box<dyn Error>> {
        debug!("Loading module: {:?}", mdl.name());

        mdl.init_logger(if cfg.cfg.preferences.debug {
            log::LevelFilter::Debug
        } else {
            log::LevelFilter::Info
        });
        let module_name = mdl.name();
        mdl.on_load(db, cfg).await;
        let mut meta = ModuleInformation {
            name: module_name.to_owned(),
            commands: Vec::new(),
        };

        for cmd in mdl.commands() {
            let cmd_name = cmd.command_name().to_lowercase();
            debug!("Adding {} command to the manager.", cmd_name);
            for alias in cmd.aliases().clone() {
                self.aliases.insert(alias.to_owned(), cmd_name.clone());
            }
            cmd.on_load(db, cfg).await;
            let command_info = cmd.information();
            self.commands.insert(cmd_name, cmd);
            meta.commands.push(command_info);
        }
        for (index, react) in mdl.reactions().into_iter().enumerate() {
            react.on_load(db, cfg).await;
            let emojis = react.emojis();
            match emojis {
                None => {
                    debug!("Adding unfiltered reaction handler to the manager.");
                    self.add_reaction_mapping("None", index);
                }
                Some(icons) => {
                    debug!("Adding reactions ({}) to the manager.", icons.join(", "));
                    for icon in icons {
                        self.add_reaction_mapping(icon, index);
                    }
                }
            }
            self.reactions.insert(index, react);
        }
        self.information.insert(module_name.to_lowercase(), meta);
        info!("Loaded the {} module.", module_name);
        Ok(())
    }

    pub fn find_command<S>(&self, lookup: S) -> Option<&Box<dyn UnicornCommand>>
    where
        S: AsRef<str>,
    {
        debug!("Looking up command: {}...", lookup.as_ref());

        let cmd_name = match self.aliases.get(&lookup.as_ref().to_lowercase()) {
            Some(val) => val.to_owned(),
            None => lookup.as_ref().to_lowercase(),
        };

        self.commands.get(&cmd_name).inspect(|_cmd| {
            debug!("Found command {}.", cmd_name);
        })
    }

    fn add_reaction_mapping(&mut self, key: &str, target: usize) {
        self.reaction_map
            .entry(key.to_owned())
            .or_default();
        self.reaction_map
            .get_mut(key)
            .unwrap()
            .push(target);
    }

    pub fn find_reaction<S>(&self, lookup: S) -> Option<Vec<&Box<dyn UnicornReaction>>>
    where
        S: AsRef<str>,
    {
        debug!("Looking up reaction: {}...", lookup.as_ref());
        let mut targets = Vec::new();
        if let Some(filtered) = self.reaction_map.get(lookup.as_ref()) {
            for react in filtered {
                targets.push(react);
            }
        }
        if let Some(unfiltered) = self.reaction_map.get("None") {
            for react in unfiltered {
                targets.push(react);
            }
        }
        let mut result = Vec::new();
        for i in targets {
            if let Some(target) = self.reactions.get(i) {
                result.push(target);
            }
        }
        if !result.is_empty() {
            Some(result)
        } else {
            None
        }
    }
}
