use std::collections::HashMap;
use std::sync::Arc;

use log::error;
use serde::de::DeserializeOwned;

use crate::common::ConfigLoader;
use crate::config::Configuration;
use crate::custom::CustomConfig;

pub struct ConfigurationBuilder {
    pub cfg: Arc<Configuration>,
    map: HashMap<String, CustomConfig>,
}

impl ConfigurationBuilder {
    pub fn new(cfg: Arc<Configuration>) -> Self {
        Self {
            map: HashMap::new(),
            cfg,
        }
    }

    pub fn add<T>(&mut self)
    where
        T: ConfigLoader + DeserializeOwned,
    {
        let cfg = T::new();
        let custom = CustomConfig::new(Box::new(cfg.unwrap_or_else(|why| {
            error!("{}", why);
            T::default()
        })));
        let name = std::any::type_name::<T>();
        self.map.insert(name.to_owned(), custom);
    }

    pub fn collect_custom(&self) -> HashMap<String, CustomConfig> {
        self.map.clone()
    }
}
