use std::any::Any;

use log::info;
use serde::Deserialize;

use crate::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct CurrencyName {
    pub single: String,
    pub plural: String,
}

impl CurrencyName {
    pub fn new(single: String, plural: String) -> Self {
        Self { single, plural }
    }
    pub fn to_string(&self) -> String {
        format!("{}/{}", &self.single, &self.plural)
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct CurrencyEntry {
    pub name: CurrencyName,
    pub icon: String,
}

impl CurrencyEntry {
    pub fn new(icon: String, single: String, plural: String) -> Self {
        let name = CurrencyName::new(single, plural);
        Self { name, icon }
    }

    pub fn to_string(&self) -> String {
        format!("{} {}", &self.icon, &self.name.to_string())
    }

    pub fn describe(&self, amt: isize, symbol_after: bool) -> String {
        if symbol_after {
            format!("{} {}{}", amt, &self.name.to_string(), &self.icon)
        } else {
            format!("{} {}{}", amt, self.icon, &self.name.to_string())
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct CurrencyConfig {
    #[serde(default = "CurrencyConfig::default_common")]
    pub common: CurrencyEntry,
    #[serde(default = "CurrencyConfig::default_premium")]
    pub premium: CurrencyEntry,
    #[serde(default = "CurrencyConfig::default_reputation")]
    pub reputation: CurrencyEntry,
}

impl CurrencyConfig {
    fn default_common() -> CurrencyEntry {
        let icon = std::env::var(Self::env_name("common_icon".to_owned()))
            .unwrap_or_else(|_| "⚜".to_owned());
        let single = std::env::var(Self::env_name("common_name_single".to_owned()))
            .unwrap_or_else(|_| "Kud".to_owned());
        let plural = std::env::var(Self::env_name("common_name_plural".to_owned()))
            .unwrap_or_else(|_| "Kud".to_owned());
        CurrencyEntry::new(icon, single, plural)
    }

    fn default_premium() -> CurrencyEntry {
        let icon = std::env::var(Self::env_name("premium_icon".to_owned()))
            .unwrap_or_else(|_| "Σ".to_owned());
        let single = std::env::var(Self::env_name("premium_name_single".to_owned()))
            .unwrap_or_else(|_| "Sumarum".to_owned());
        let plural = std::env::var(Self::env_name("premium_name_plural".to_owned()))
            .unwrap_or_else(|_| "Sumarum".to_owned());
        CurrencyEntry::new(icon, single, plural)
    }

    fn default_reputation() -> CurrencyEntry {
        let icon = std::env::var(Self::env_name("reputation_icon".to_owned()))
            .unwrap_or_else(|_| "🍪".to_owned());
        let single = std::env::var(Self::env_name("reputation_name_single".to_owned()))
            .unwrap_or_else(|_| "Cookies".to_owned());
        let plural = std::env::var(Self::env_name("reputation_name_plural".to_owned()))
            .unwrap_or_else(|_| "Cookies".to_owned());
        CurrencyEntry::new(icon, single, plural)
    }
}

impl ConfigLoader for CurrencyConfig {
    fn file_name() -> String {
        "preferences_currency".to_owned()
    }

    fn default() -> Self {
        Self {
            common: Self::default_common(),
            premium: Self::default_premium(),
            reputation: Self::default_reputation(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!("Common: {}", &self.common.to_string());
        info!("Premium: {}", &self.premium.to_string());
        info!("Reputation: {}", &self.reputation.to_string());
    }
}
