use std::any::Any;

use log::info;
use serde::Deserialize;

use crate::common::ConfigLoader;

#[derive(Debug, Clone, Deserialize)]
pub struct CacheConfig {
    #[serde(default = "CacheConfig::default_flavor")]
    pub flavor: String,
    #[serde(default = "CacheConfig::default_host")]
    pub host: String,
    #[serde(default = "CacheConfig::default_port")]
    pub port: u16,
    #[serde(default = "CacheConfig::default_ttl")]
    pub ttl: Option<u64>,
    #[serde(default = "CacheConfig::default_db")]
    pub db: u8,
}

impl ConfigLoader for CacheConfig {
    fn file_name() -> String {
        "cache".to_owned()
    }

    fn default() -> Self {
        Self {
            flavor: Self::default_flavor(),
            host: Self::default_host(),
            port: Self::default_port(),
            ttl: Self::default_ttl(),
            db: Self::default_db(),
        }
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn describe(&self) {
        info!("Flavor: {}", &self.flavor);
        if &self.flavor == "redis" {
            info!("Address: {}:{}/{}", &self.host, &self.port, &self.db);
        }
    }
}

impl CacheConfig {
    fn default_flavor() -> String {
        std::env::var(Self::env_name("flavor".to_owned())).unwrap_or_else(|_| "memory".to_owned())
    }

    fn default_host() -> String {
        std::env::var(Self::env_name("host".to_owned())).unwrap_or_else(|_| "localhost".to_owned())
    }

    fn default_port() -> u16 {
        let default = 6379u16;
        match std::env::var(Self::env_name("port".to_owned())) {
            Ok(port_str) => port_str.parse::<u16>().unwrap_or(default),
            Err(_) => default,
        }
    }

    fn default_ttl() -> Option<u64> {
        let default = None;
        match std::env::var(Self::env_name("ttl".to_owned())) {
            Ok(ttl_str) => match ttl_str.parse::<u64>() {
                Ok(ttl) => {
                    if ttl == 0 {
                        None
                    } else {
                        Some(ttl)
                    }
                }
                Err(_) => default,
            },
            Err(_) => default,
        }
    }

    fn default_db() -> u8 {
        let default = 3u8;
        match std::env::var(Self::env_name("port".to_owned())) {
            Ok(port_str) => port_str.parse::<u8>().unwrap_or(default),
            Err(_) => default,
        }
    }
}
