use std::fmt::{Display, Formatter, Result};

pub enum UnicornConfigError {
    IO(std::io::Error),
    SerdeYAML(serde_yaml::Error),
    SerdeJSON(serde_json::Error),
}

impl From<std::io::Error> for UnicornConfigError {
    fn from(err: std::io::Error) -> Self {
        UnicornConfigError::IO(err)
    }
}

impl From<serde_yaml::Error> for UnicornConfigError {
    fn from(err: serde_yaml::Error) -> Self {
        UnicornConfigError::SerdeYAML(err)
    }
}

impl From<serde_json::Error> for UnicornConfigError {
    fn from(err: serde_json::Error) -> Self {
        UnicornConfigError::SerdeJSON(err)
    }
}

impl Display for UnicornConfigError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        match *self {
            UnicornConfigError::IO(ref err) => write!(f, "{}", err),
            UnicornConfigError::SerdeYAML(ref err) => write!(f, "{})", err),
            UnicornConfigError::SerdeJSON(ref err) => write!(f, "{})", err),
        }
    }
}
