use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct UnflipCommand;

impl UnflipCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UnflipCommand {
    fn command_name(&self) -> &str {
        "unflip"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn description(&self) -> &str {
        "Toggles if any flipped tables should be unflipped (and sometimes thrown)."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(perms) = pld.msg.author_permissions(&pld.ctx.cache) {
            perms.manage_guild()
        } else {
            false
        }
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if let Some(mut settings) = pld.settings.clone() {
            settings.unflip = !settings.unflip;
            settings.save(&pld.db).await;
            let state = if settings.unflip {
                "disabled"
            } else {
                "enabled"
            };
            UnicornEmbed::ok(format!("Table unflipping has been {}.", state))
        } else {
            UnicornEmbed::error("Failed getting the guild's settings!")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
