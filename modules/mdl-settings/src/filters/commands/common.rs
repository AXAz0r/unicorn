pub struct FilterCommon;

impl FilterCommon {
    fn safe_args(args: Vec<String>) -> Vec<String> {
        let mut words = Vec::<String>::new();
        for arg in args {
            let pieces: Vec<&str> = arg.split(' ').collect();
            for piece in pieces {
                words.push(piece.to_lowercase());
            }
        }
        words
    }

    pub fn safe_addition(iterable: &mut Vec<String>, args: Vec<String>, safe: bool) -> u64 {
        let mut count = 0;
        let arg_words = if safe { Self::safe_args(args) } else { args };
        for aw in arg_words {
            if !iterable.contains(&aw) {
                iterable.push(aw);
                count += 1;
            }
        }
        count
    }
}
