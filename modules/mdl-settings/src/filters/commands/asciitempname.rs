use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct ASCIITempNameCommand;

impl ASCIITempNameCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ASCIITempNameCommand {
    fn command_name(&self) -> &str {
        "asciitempname"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["asciitemp"]
    }

    fn description(&self) -> &str {
        "Changes the default temporary name for those who the temporary ASCII name was enforced on.\
        The enforcement tries to clean the user's name,\
        but if there's nothing left, this is placed instead."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(perms) = pld.msg.author_permissions(&pld.ctx.cache) {
            perms.manage_guild()
        } else {
            false
        }
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("temp_name", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if pld.args.has_argument("temp_name") {
            if let Some(mut settings) = pld.settings.clone() {
                let temp_name = pld.args.get("temp_name");
                settings.ascii_temp_name = Some(temp_name.to_owned());
                settings.save(&pld.db).await;
                UnicornEmbed::ok("New ASCII enforcement fallback name set.")
            } else {
                UnicornEmbed::error("Failed getting the guild's settings!")
            }
        } else {
            UnicornEmbed::info(format!(
                "`{}` is the current fallback name.",
                if let Some(settings) = pld.settings.clone() {
                    if let Some(temp_name) = settings.ascii_temp_name {
                        temp_name
                    } else {
                        "<ASCII Only: Change My Name>".to_owned()
                    }
                } else {
                    "[GUILD SETTINGS ERROR]".to_owned()
                }
            ))
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
