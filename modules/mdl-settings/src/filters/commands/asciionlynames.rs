use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct ASCIIOnlyNamesCommand;

impl ASCIIOnlyNamesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ASCIIOnlyNamesCommand {
    fn command_name(&self) -> &str {
        "asciionlynames"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["forceascii"]
    }

    fn description(&self) -> &str {
        "Toggles if only ASCII characters are allowed in names.\
        The bot will check members' names when they join or update their profile\
        for any non ASCII characters and rename them if found.\
        To change the default temporary name, use the \"asciitempname\" command."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(perms) = pld.msg.author_permissions(&pld.ctx.cache) {
            perms.manage_guild()
        } else {
            false
        }
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if let Some(mut settings) = pld.settings.clone() {
            settings.ascii_only_names = !settings.ascii_only_names;
            settings.save(&pld.db).await;
            let state = if settings.ascii_only_names {
                "disabled"
            } else {
                "enabled"
            };
            UnicornEmbed::ok(format!("ASCII name enforcement has been {}.", state))
        } else {
            UnicornEmbed::error("Failed getting the guild's settings!")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
