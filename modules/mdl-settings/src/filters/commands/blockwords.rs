use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::filters::commands::common::FilterCommon;

#[derive(Default)]
pub struct BlockWordsCommand;

impl BlockWordsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for BlockWordsCommand {
    fn command_name(&self) -> &str {
        "blockwords"
    }

    fn category(&self) -> &str {
        "settings"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["blockword", "filterwords", "filterword"]
    }

    fn description(&self) -> &str {
        "Adds all the words you list to the blocked words filter.\
        If any of the words in the filter are sent, the message will be deleted and the author will be notified.\
        Words should not be separated by a delimiter. Those with the Administrator permission are not affected.\
        These will block individual words, not phrases or combinations of words."
    }

    async fn discord_permissions(&self, pld: &mut CommandPayload) -> bool {
        if let Some(perms) = pld.msg.author_permissions(&pld.ctx.cache) {
            perms.manage_guild()
        } else {
            false
        }
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if !pld.args.raw().is_empty() {
            if let Some(mut settings) = pld.settings.clone() {
                let count =
                    FilterCommon::safe_addition(&mut settings.blocked_words, pld.args.raw(), true);
                settings.save(&pld.db).await;
                if count != 0 {
                    UnicornEmbed::ok(format!(
                        "Added {} new blocked words. There are {} in total.",
                        count,
                        settings.blocked_words.len()
                    ))
                } else {
                    UnicornEmbed::warn(
                        "All of the entered words are already blocked, so they have been ignored.",
                    )
                }
            } else {
                UnicornEmbed::error("Failed getting the guild's settings!")
            }
        } else {
            UnicornEmbed::error("No words to block given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
