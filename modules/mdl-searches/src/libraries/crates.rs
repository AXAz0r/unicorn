use chrono::FixedOffset;
use serde::Deserialize;
use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateEmbedFooter, CreateMessage};
use serenity::model::Timestamp;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_http::client::UnicornHttpClient;

const CRATES_API: &str = "https://crates.io/api/v1/crates";
const CRATES_ICON: &str = "https://i.imgur.com/Nyw7kSc.png";
const CRATES_COLOR: u64 = 0x3a_64_36;
const CRATES_DATE_FMT: &str = "%+";

#[derive(Clone, Debug, Deserialize)]
pub struct CrateLinks {
    pub owners: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct OwnersResponse {
    pub users: Vec<CrateUser>,
}

impl CrateLinks {
    pub async fn get_owners(&self) -> anyhow::Result<OwnersResponse> {
        let url = format!("https://crates.io{}", &self.owners);
        let client = UnicornHttpClient::new()?;
        let resp = client.get(&url).await?;
        let body = resp.text().await?;
        Ok(serde_json::from_str(&body)?)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct Crate {
    pub id: String,
    pub name: String,
    pub keywords: Vec<String>,
    created_at: String,
    pub downloads: u64,
    pub newest_version: String,
    pub description: String,
    pub links: CrateLinks,
}

impl Crate {
    pub fn created_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.created_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateVersionLinks {
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateUser {
    pub login: String,
    pub avatar: String,
    pub url: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct AuditAction {
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateVersion {
    pub id: u64,
    pub num: String,
    updated_at: String,
}

impl CrateVersion {
    pub fn updated_at(&self) -> Option<chrono::DateTime<FixedOffset>> {
        CratesCommand::datetime(&self.updated_at)
    }
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateKeyword {
}

#[derive(Clone, Debug, Deserialize)]
pub struct CrateCategory {
}

#[derive(Clone, Debug, Deserialize)]
pub struct CratesResponse {
    #[serde(rename = "crate")]
    pub pack: Crate,
    pub versions: Vec<CrateVersion>,
    pub keywords: Vec<CrateKeyword>,
}

impl CratesResponse {
    pub async fn new(lookup: impl ToString) -> anyhow::Result<CratesResponse> {
        let url = format!("{}/{}", CRATES_API, lookup.to_string());
        let client = UnicornHttpClient::new()?;
        let resp = client.get(&url).await?;
        let body = resp.text().await?;
        Ok(serde_json::from_str(&body)?)
    }

    pub fn url(&self) -> String {
        format!("https://crates.io/crates/{}", &self.pack.id)
    }

    pub fn latest(&self) -> CrateVersion {
        let mut best = self.versions[0].clone();
        for ver in &self.versions {
            if ver.num == self.pack.newest_version {
                best = ver.clone();
                break;
            } else if ver.id > best.id {
                best = ver.clone();
            }
        }
        best
    }
}

#[derive(Default)]
pub struct CratesCommand;

impl CratesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn datetime(text: impl ToString) -> Option<chrono::DateTime<FixedOffset>> {
        if let Ok(dt) = chrono::DateTime::parse_from_str(&text.to_string(), CRATES_DATE_FMT) {
            Some(dt)
        } else {
            None
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for CratesCommand {
    fn command_name(&self) -> &str {
        "crates"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["crate", "cargo"]
    }

    fn example(&self) -> &str {
        "javelin"
    }

    fn description(&self) -> &str {
        "Search Rust's/Cargo's package repository on crates.io \
         for the specified package and displays its details."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("crate", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if pld.args.has_argument("crate") {
            let lookup = pld.args.get("crate");
            match CratesResponse::new(lookup).await {
                Ok(pack) => {
                    let owner_result = pack.pack.links.get_owners().await;
                    let (owners, owner_ender) = match &owner_result {
                        Ok(ors) => {
                            let mut stack = Vec::<String>::new();
                            for usr in &ors.users {
                                stack.push(format!("[{}]({})", &usr.login, &usr.url));
                            }
                            (stack.join(", "), if stack.len() == 1 { "" } else { "s" })
                        }
                        Err(_) => ("Couldn\'t retrieve owners.".to_owned(), ""),
                    };
                    let latest = pack.latest();
                    let author =
                        CreateEmbedAuthor::new(format!("{} {}", &pack.pack.name, &latest.num))
                            .url(pack.url())
                            .icon_url(CRATES_ICON);
                    let footer = CreateEmbedFooter::new(if !pack.keywords.is_empty() {
                        format!("Keywords: {}", pack.pack.keywords.join(", "))
                    } else {
                        "Last updated".to_owned()
                    });
                    let info = format!(
                        "Owner{}: {}\nCreated: {} UTC\nDownloads: {}\n**{}**",
                        owner_ender,
                        owners,
                        if let Some(ts) = pack.pack.created_at() {
                            ts.format("%Y-%m-%d %H:%M:%S").to_string()
                        } else {
                            "Unknown".to_owned()
                        },
                        pack.pack.downloads,
                        pack.pack.description
                    );
                    let mut embed = CreateEmbed::default()
                        .author(author)
                        .footer(footer)
                        .color(CRATES_COLOR)
                        .description(info);
                    if let Some(uat) = latest.updated_at() {
                        if let Ok(stamp) = Timestamp::parse(&uat.to_rfc3339()) {
                            embed = embed.timestamp(stamp);
                        }
                    }
                    if let Ok(ow) = owner_result {
                        if let Some(usr) = ow.users.first() {
                            embed = embed.thumbnail(&usr.avatar);
                        }
                    }
                    CreateMessage::default().embed(embed)
                }
                Err(why) => UnicornEmbed::error(format!("Failed to grab the crate: {}.", why)),
            }
        } else {
            UnicornEmbed::error("No crate name given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
