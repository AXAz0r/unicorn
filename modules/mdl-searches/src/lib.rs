use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::libraries::crates::CratesCommand;
use crate::media::deezer::DeezerCommand;
use crate::{kitsu::anime::AnimeCommand, kitsu::manga::MangaCommand};

// mod dictionary;
mod kitsu;
mod libraries;
mod media;
// mod other;

#[derive(Default)]
pub struct SearchModule;

impl SearchModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornModule for SearchModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            DeezerCommand::boxed(),
            AnimeCommand::boxed(),
            MangaCommand::boxed(),
            CratesCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Searches"
    }
}
