use serenity::all::CreateEmbedFooter;
use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::kitsu::common::{KitsuMode, KitsuResponse, KITSU_COLOR, KITSU_ICON};

#[derive(Default)]
pub struct MangaCommand;

impl MangaCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for MangaCommand {
    fn command_name(&self) -> &str {
        "manga"
    }

    fn category(&self) -> &str {
        "searches"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["mango", "kitsumanga"]
    }

    fn example(&self) -> &str {
        "A Silent Voice"
    }

    fn description(&self) -> &str {
        "Searches Kitsu.io for the specified manga. \
         The outputed results will be information like the number of chapters, \
         user rating, plot summary, and poster image."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            match KitsuResponse::new(lookup, KitsuMode::Manga).await {
                Ok(qry) => {
                    if let Some(manga) = qry.best(lookup) {
                        let author =
                            CreateEmbedAuthor::new(manga.en_title().unwrap_or_else(|| {
                                manga
                                    .ja_title()
                                    .unwrap_or_else(|| "<Unprocessable Name>".to_owned())
                            }))
                            .icon_url(KITSU_ICON)
                            .url(manga.url());
                        let footer = CreateEmbedFooter::new(
                            "Click the title at the top to see the page of the manga.",
                        );
                        let info =
                            format!(
                            "Title: {}\nRating: {}%\nAir Time: {} - {}\nVolumes: {}\nChapters: {}",
                            manga.ja_title().unwrap_or_else(|| "<Unprocessable Name>".to_owned()),
                            manga.attrs.rating(),
                            manga.attrs.start_date.unwrap_or_else(|| "???".to_owned()),
                            manga.attrs.end_date.unwrap_or_else(|| "???".to_owned()),
                            match manga.attrs.volume_count {
                                Some(cnt) => cnt.to_string(),
                                None => "Unknown".to_owned(),
                            },
                            match manga.attrs.chapter_count {
                                Some(len) => format!("{} Minutes", len),
                                None => "Unknown".to_owned(),
                            }
                        );
                        let mut embed = CreateEmbed::default()
                            .color(KITSU_COLOR)
                            .author(author)
                            .footer(footer)
                            .field("Information", info, false)
                            .field("Synopsis", manga.attrs.synopsis.split_at(384).0, false);
                        if let Some(imgs) = manga.attrs.poster_image {
                            embed = embed.thumbnail(imgs.original);
                        }
                        CreateMessage::default().embed(embed)
                    } else {
                        UnicornEmbed::not_found("Nothing found for that lookup.")
                    }
                }
                Err(why) => UnicornEmbed::error(format!("Kitsu manga lookup failed: {}", why)),
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
