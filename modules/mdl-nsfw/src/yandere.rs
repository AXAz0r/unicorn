use log::debug;
use serenity::builder::{CreateAttachment, CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::common::danbooru::DanbooruEntry;
use crate::common::util::Utility;

const API_BASE: &str = "https://yande.re/post.json?limit=100";
const URL_BASE: &str = "https://yande.re/post/show";
const API_ICON: &str = "https://i.imgur.com/vgJwau2.png";
const API_NAME: &str = "Yandere";
const RESP_CLR: u64 = 0xad_3d_3d;

const FAIL_ERR: &str = "Failed to communicate with Yandere.";
const EMPTY_ERR: &str = "This lookup gave no results.";

#[derive(Default)]
pub struct YandereCommand;

impl YandereCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for YandereCommand {
    fn command_name(&self) -> &str {
        "yandere"
    }

    fn category(&self) -> &str {
        "nsfw"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["yre"]
    }

    fn example(&self) -> &str {
        "naked_apron"
    }

    fn description(&self) -> &str {
        "Searches Yandere for the specified tag. \
         If no tag is given, the latest posts will be used. \
         Separate different tags with a space and replace spaces \
         within a single tag with underscores \"_\"."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("tags", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let key = Utility::cache_key(
            self.category(),
            self.command_name(),
            Utility::tag_key(pld.args.get("tags").split(' ').collect()),
        );
        let url = if pld.args.is_empty() {
            API_BASE.to_owned()
        } else {
            format!(
                "{}&tags={}",
                API_BASE,
                pld.args.get("tags").replace(" ", "+")
            )
        };
        let entries = match pld.cache.get(key.clone()).await {
            Some(body) => {
                if let Ok(items) = DanbooruEntry::from_body(body) {
                    if !items.is_empty() {
                        Ok(items)
                    } else {
                        DanbooruEntry::new(&url).await
                    }
                } else {
                    DanbooruEntry::new(&url).await
                }
            }
            None => DanbooruEntry::new(&url).await,
        };
        let response = match entries {
            Ok(mut dse) => {
                if !dse.is_empty() {
                    if let Some(dsi) = DanbooruEntry::random_item(&mut dse) {
                        if let Ok(serbody) = DanbooruEntry::vec_to_body(dse) {
                            pld.cache.set(key, serbody).await;
                        }
                        let post = format!("{}/{}", URL_BASE, dsi.id);
                        let mut r = CreateMessage::default();
                        let img_name = Utility::parse_img_name(&dsi.file_url, pld.msg.id.get());
                        let downloaded = match Utility::fetch_img_bytes(&dsi.file_url).await {
                            Ok(bytes) => {
                                if bytes.len() < 8_388_608 {
                                    let attachment = CreateAttachment::bytes(bytes, img_name.clone());
                                    r = r.add_file(attachment);
                                    true
                                } else {
                                    debug!("Image too large to upload to discord.");
                                    false
                                }
                            }
                            Err(why) => {
                                debug!("Image byte pulling failed: {}", why);
                                false
                            }
                        };
                        let author = CreateEmbedAuthor::new(format!("{}: {}", API_NAME, dsi.id)).icon_url(API_ICON).url(post);
                        let mut embed = CreateEmbed::default().color(RESP_CLR).author(author);
                        if downloaded {
                            embed = embed.image(format!("attachment://{}", &img_name));
                        } else {
                            embed = embed.image(dsi.file_url);
                        }
                        r = r.embed(embed);
                        r
                    } else {
                        UnicornEmbed::error(EMPTY_ERR)
                    }
                } else {
                    UnicornEmbed::error(EMPTY_ERR)
                }
            }
            Err(why) => {
                debug!("Failed getting Yandere posts: {}", why);
                UnicornEmbed::error(FAIL_ERR)
            }
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
