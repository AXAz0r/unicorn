use log::debug;
use serenity::all::CreateEmbedFooter;
use serenity::builder::{CreateAttachment, CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::common::open_api::OpenAPIEntry;
use crate::common::util::Utility;

const API_BASE: &str = "http://api.oboobs.ru/boobs";
const IMG_BASE: &str = "http://media.oboobs.ru";
const API_ICON: &str = "https://i.imgur.com/vmjthbd.png";
const API_NAME: &str = "Open Boobs";
const MAX_ROLL: u64 = 13636;
const RESP_CLR: u64 = 0xf9_f9_f9;

const NUM_ERR: &str = "Invalid image number given.";
const FAIL_ERR: &str = "Failed to communicate with the boob service.";
const EMPTY_ERR: &str = "This lookup gave no results.";

#[derive(Default)]
pub struct BoobsCommand;

impl BoobsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for BoobsCommand {
    fn command_name(&self) -> &str {
        "boobs"
    }

    fn category(&self) -> &str {
        "nsfw"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["tits"]
    }

    fn description(&self) -> &str {
        "Outputs a random NSFW image focusing on the breasts of the model. \
         You can specify the ID number of the image if you want a specific one."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("index", true)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let num = OpenAPIEntry::fetch_num(&pld.args, MAX_ROLL);
        let response = match num {
            Some(num) => {
                let key = Utility::cache_key(self.category(), self.command_name(), num);
                let url = format!("{}/{}", API_BASE, num);
                let (fresh, oae) = match OpenAPIEntry::from_cache(pld, key.clone()).await {
                    Ok(oae) => match oae {
                        Some(oae) => (false, Ok(Some(oae))),
                        None => (true, OpenAPIEntry::new(&url).await),
                    },
                    Err(_) => (true, OpenAPIEntry::new(&url).await),
                };
                match oae {
                    Ok(oar) => {
                        if let Some(oar) = oar {
                            if fresh {
                                oar.save_cache(pld, key).await;
                            }
                            let mut r = CreateMessage::default();
                            let img_name = Utility::parse_img_name(
                                &oar.get_image_url(IMG_BASE),
                                pld.msg.id.get(),
                            );
                            let downloaded = match Utility::fetch_img_bytes(
                                &oar.get_image_url(IMG_BASE),
                            )
                            .await
                            {
                                Ok(bytes) => {
                                    if bytes.len() < 8_388_608 {
                                        let attachment =
                                            CreateAttachment::bytes(bytes, img_name.clone());
                                        r = r.add_file(attachment);
                                        true
                                    } else {
                                        debug!("Image too large to upload to discord.");
                                        false
                                    }
                                }
                                Err(why) => {
                                    debug!("Image byte pulling failed: {}", why);
                                    false
                                }
                            };
                            let author = CreateEmbedAuthor::new(API_NAME).icon_url(API_ICON);
                            let footer = CreateEmbedFooter::new(format!(
                                "ID: {} | Model: {} | Ranking: {}",
                                num,
                                oar.get_model_name(),
                                oar.rank
                            ));
                            let mut embed = CreateEmbed::default()
                                .color(RESP_CLR)
                                .author(author)
                                .footer(footer);
                            if downloaded {
                                embed = embed.image(format!("attachment://{}", &img_name));
                            } else {
                                embed = embed.image(oar.get_image_url(IMG_BASE));
                            }
                            r = r.embed(embed);
                            r
                        } else {
                            UnicornEmbed::error(EMPTY_ERR)
                        }
                    }
                    Err(why) => {
                        debug!("Failed getting {} image: {}", self.command_name(), why);
                        UnicornEmbed::error(FAIL_ERR)
                    }
                }
            }
            None => UnicornEmbed::error(NUM_ERR),
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
