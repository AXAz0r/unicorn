use unicorn_http::client::UnicornHttpClient;
use unicorn_utility::hash::UnicornHasher;

pub struct Utility;

impl Utility {
    pub fn cache_key(cat: &str, name: &str, num: u64) -> String {
        format!("cmd:{}:{}:{}", cat, name, num)
    }

    pub fn sort_tags(mut tags: Vec<&str>) -> Vec<&str> {
        tags.sort_unstable();
        tags
    }

    pub fn tag_key(tags: Vec<&str>) -> u64 {
        let sorted = Self::sort_tags(tags);
        let stringed = sorted.join("-");
        UnicornHasher::calculate_u64_hash(&stringed)
    }

    pub fn parse_img_name(url: &str, mid: u64) -> String {
        let mut pieces = url.split('.').collect::<Vec<&str>>();
        let ext = pieces.remove(pieces.len() - 1);
        format!("{}.{}", mid, ext)
    }

    pub async fn fetch_img_bytes(url: &str) -> anyhow::Result<Vec<u8>> {
        let client = UnicornHttpClient::new()?;
        let response = client.get(url).await?;
        let bytes = response.bytes().await?;
        Ok(bytes.to_vec())
    }
}
