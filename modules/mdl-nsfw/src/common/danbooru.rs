use rand::Rng;
use serde::{Deserialize, Serialize};
use unicorn_http::client::UnicornHttpClient;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct DanbooruEntry {
    pub id: u64,
    pub file_url: String,
}

impl DanbooruEntry {
    pub async fn new(url: &str) -> anyhow::Result<Vec<Self>> {
        let cli = UnicornHttpClient::new()?;
        let resp = cli.get(url).await?;
        let body = resp.text().await?;
        Self::from_body(body)
    }

    pub fn from_body(body: String) -> anyhow::Result<Vec<Self>> {
        let items = serde_json::from_str::<Vec<DanbooruEntry>>(&body)?;
        Ok(items)
    }

    pub fn vec_to_body(items: Vec<Self>) -> anyhow::Result<String> {
        Ok(serde_json::to_string(&items)?)
    }

    pub fn random_item(items: &mut Vec<Self>) -> Option<Self> {
        if items.is_empty() {
            None
        } else {
            let mut rng = rand::thread_rng();
            let index = rng.gen_range(0..items.len());
            Some(items.remove(index))
        }
    }
}
