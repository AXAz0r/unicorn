use rand::Rng;
use serde::{Deserialize, Serialize};

use unicorn_callable::payload::CommandPayload;
use unicorn_http::client::UnicornHttpClient;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct XMLBooruWrapper {
    #[serde(rename = "post")]
    pub posts: Vec<XMLBooruEntry>,
}

impl XMLBooruWrapper {
    pub async fn fresh_items(
        api_base: &str,
        pld: &CommandPayload,
    ) -> anyhow::Result<Vec<XMLBooruEntry>> {
        let url = if pld.args.is_empty() {
            api_base.to_owned()
        } else {
            format!(
                "{}&tags={}",
                api_base,
                pld.args.get("tags").replace(" ", "+")
            )
        };
        XMLBooruEntry::new(&url).await
    }

    pub fn vec_to_body(items: Vec<XMLBooruEntry>) -> Result<String, serde_json::Error> {
        serde_json::to_string(&items)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct XMLBooruEntry {
    pub id: u64,
    pub file_url: String,
}

impl XMLBooruEntry {
    pub async fn new(url: &str) -> anyhow::Result<Vec<Self>> {
        let cli = UnicornHttpClient::new()?;
        let resp = cli.get(url).await?;
        let body = resp.text().await?;
        Self::from_body(body)
    }

    pub fn from_body(body: String) -> anyhow::Result<Vec<Self>> {
        let wrapper = serde_xml_rs::from_str::<XMLBooruWrapper>(&body)?;
        Ok(wrapper.posts)
    }

    pub fn from_json(body: String) -> anyhow::Result<Vec<Self>> {
        let posts = serde_json::from_str::<Vec<Self>>(&body)?;
        Ok(posts)
    }

    pub fn random_item(items: &mut Vec<Self>) -> Option<Self> {
        if items.is_empty() {
            None
        } else {
            let mut rng = rand::thread_rng();
            let index = rng.gen_range(0..items.len());
            Some(items.remove(index))
        }
    }
}
