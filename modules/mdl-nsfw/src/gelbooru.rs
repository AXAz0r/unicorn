use log::debug;
use serenity::builder::{CreateAttachment, CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::common::util::Utility;
use crate::common::xmlbooru::{XMLBooruEntry, XMLBooruWrapper};

const API_BASE: &str = "https://gelbooru.com/index.php?page=dapi&s=post&q=index";
const URL_BASE: &str = "https://gelbooru.com/index.php?page=post&s=view";
const API_ICON: &str = "https://gelbooru.com/favicon.ico";
const API_NAME: &str = "Gelbooru";
const RESP_CLR: u64 = 0x00_6f_fa;

const FAIL_ERR: &str = "Failed to communicate with Gelbooru.";
const EMPTY_ERR: &str = "This lookup gave no results.";

#[derive(Default)]
pub struct GelbooruCommand;

impl GelbooruCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GelbooruCommand {
    fn command_name(&self) -> &str {
        "gelbooru"
    }

    fn category(&self) -> &str {
        "nsfw"
    }

    fn nsfw(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["gelb"]
    }

    fn example(&self) -> &str {
        "ovum"
    }

    fn description(&self) -> &str {
        "Searches Gelbooru for the specified tag. \
         If no tag is given, the latest posts will be used. \
         Separate different tags with a space and replace spaces \
         within a single tag with underscores \"_\"."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("tags", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let key = Utility::cache_key(
            self.category(),
            self.command_name(),
            Utility::tag_key(pld.args.get("tags").split(' ').collect()),
        );
        let entries = match pld.cache.get(key.clone()).await {
            Some(body) => {
                if let Ok(items) = XMLBooruEntry::from_json(body) {
                    if !items.is_empty() {
                        Ok(items)
                    } else {
                        XMLBooruWrapper::fresh_items(API_BASE, pld).await
                    }
                } else {
                    XMLBooruWrapper::fresh_items(API_BASE, pld).await
                }
            }
            None => XMLBooruWrapper::fresh_items(API_BASE, pld).await,
        };
        let response = match entries {
            Ok(mut dse) => {
                if !dse.is_empty() {
                    if let Some(dsi) = XMLBooruEntry::random_item(&mut dse) {
                        if let Ok(serbody) = XMLBooruWrapper::vec_to_body(dse) {
                            pld.cache.set(key, serbody).await;
                        }
                        let post = format!("{}/{}", URL_BASE, dsi.id);
                        let mut r = CreateMessage::default();
                        let img_name = Utility::parse_img_name(&dsi.file_url, pld.msg.id.get());
                        let downloaded = match Utility::fetch_img_bytes(&dsi.file_url).await {
                            Ok(bytes) => {
                                if bytes.len() < 8_388_608 {
                                    let attachment = CreateAttachment::bytes(bytes, img_name.clone());
                                    r = r.add_file(attachment);
                                    true
                                } else {
                                    debug!("Image too large to upload to discord.");
                                    false
                                }
                            }
                            Err(why) => {
                                debug!("Image byte pulling failed: {}", why);
                                false
                            }
                        };
                        let author = CreateEmbedAuthor::new(format!("{}: {}", API_NAME, dsi.id)).icon_url(API_ICON).url(post);
                        let mut embed = CreateEmbed::default().color(RESP_CLR).author(author);
                        if downloaded {
                            embed = embed.image(format!("attachment://{}", &img_name));
                        } else {
                            embed = embed.image(dsi.file_url);
                        }
                        r = r.embed(embed);
                        r
                    } else {
                        UnicornEmbed::error(EMPTY_ERR)
                    }
                } else {
                    UnicornEmbed::error(EMPTY_ERR)
                }
            }
            Err(why) => {
                debug!("Failed getting Gelbooru posts: {}", why);
                UnicornEmbed::error(FAIL_ERR)
            }
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
