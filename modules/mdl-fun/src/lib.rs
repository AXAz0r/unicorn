use unicorn_callable::{UnicornCommand, UnicornEvent, UnicornModule};

use crate::comics::cnh::CNHCommand;
use crate::comics::xkcd::XKCDCommand;
use crate::baking::cookies::CookiesCommand;

mod comics;
mod baking;

#[derive(Default)]
pub struct FunModule;

impl FunModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornModule for FunModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            CNHCommand::boxed(),
            XKCDCommand::boxed(),
            CookiesCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        vec![]
    }

    fn name(&self) -> &str {
        "Fun"
    }
}
