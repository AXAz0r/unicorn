use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::payload::CommandPayload;
use unicorn_resource::UnicornResource;

const LEVELER: f64 = 5.15;
const PREFIXES: [&str; 10] = [
    "Starving",
    "Picky",
    "Nibbling",
    "Munching",
    "Noming",
    "Glomping",
    "Chomping",
    "Inhaling",
    "Gobbling",
    "Devouring",
];
const SUFFIXES: [&str; 10] = [
    "Licker",
    "Taster",
    "Eater",
    "Chef",
    "Connoisseur",
    "Gorger",
    "Epicure",
    "Glutton",
    "Devourer",
    "Void",
];

#[derive(Default)]
pub struct CookiesCommand;

impl CookiesCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for CookiesCommand {
    fn command_name(&self) -> &str {
        "baking"
    }

    fn category(&self) -> &str {
        "fun"
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Shows how many baking you have, or how many a mentioned user has. \
        Cookies are given with the givecookie command."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let target = if !pld.msg.mentions.is_empty() {
            pld.msg.mentions[0].clone()
        } else {
            pld.msg.author.clone()
        };
        let resource = pld.db.get_resource("baking", target.id.get()).await;
        let ender = if resource.ranked.to_string().ends_with("1") {
            "cookie"
        } else {
            "baking"
        };
        let title = UnicornResource::get_title(resource.ranked, LEVELER, &PREFIXES, &SUFFIXES);
        let embed = CreateEmbed::default().color(0xd9_9e_82).title(format!(
            "🍪 {} the {} got {} {} this month and has {} in total.",
            target.name, title, resource.ranked, ender, resource.total
        ));
        let response = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
