use rand::Rng;
use serde::Deserialize;
use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::payload::CommandPayload;
use unicorn_http::client::UnicornHttpClient;

static COMIC_COUNT: u32 = 2597;

#[derive(Default)]
pub struct XKCDCommand;

impl XKCDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for XKCDCommand {
    fn command_name(&self) -> &str {
        "xkcd"
    }

    fn category(&self) -> &str {
        "fun"
    }

    fn description(&self) -> &str {
        "If you like humorous things and know a bit of technology, you will \
        lose a lot of time reading these. XKCD comics are perfect for procrastination \
        and time wasting."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let comic_no = XKCDComic::random();
        let comic_url = format!("https://xkcd.com/{}", comic_no);
        let comic = XKCDComic::get(&comic_url).await?;
        let embed = CreateEmbed::default()
            .color(0xf9_f9_f9)
            .title(format!("🚽 XKCD: {}", &comic.title))
            .description(&comic.alt)
            .image(&comic.img);
        let response = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}

#[derive(Deserialize)]
struct XKCDComic {
    pub title: String,
    pub alt: String,
    pub img: String,
}

impl XKCDComic {
    pub fn random() -> u32 {
        let mut rng = rand::thread_rng();
        rng.gen_range(1..COMIC_COUNT)
    }

    pub async fn get(comic_url: &str) -> anyhow::Result<Self> {
        let comic_api = format!("{}/info.0.json", comic_url);
        let client = UnicornHttpClient::new()?;
        let resp = client.get(&comic_api).await?;
        let body = resp.text().await?;
        Ok(serde_json::from_str(&body)?)
    }
}
