use rand::Rng;
use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_http::client::UnicornHttpClient;

static ICON: &str = "https://i.imgur.com/jJl7FoT.jpg";
static SELECTOR: &str = "#comic img";
static COMIC_COUNT: u32 = 6078;

#[derive(Default)]
pub struct CNHCommand;

impl CNHCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    pub fn random() -> u32 {
        let mut rng = rand::thread_rng();
        rng.gen_range(1..COMIC_COUNT)
    }

    pub async fn get_image(
        client: &UnicornHttpClient,
        selector: &scraper::Selector,
        comic_url: &str,
    ) -> anyhow::Result<Option<String>> {
        let mut comic_img = None;
        let body = client.get(comic_url).await?.text().await?;
        let root = scraper::Html::parse_document(&body);
        for element in root.select(selector) {
            let val = element.value();
            if let Some(img_url) = val.attr("src") {
                comic_img = Some(img_url.to_string());
                break;
            }
        }
        Ok(comic_img)
    }
}

#[serenity::async_trait]
impl UnicornCommand for CNHCommand {
    fn command_name(&self) -> &str {
        "cyanideandhappiness"
    }

    fn category(&self) -> &str {
        "fun"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["cnh"]
    }

    fn description(&self) -> &str {
        "Outputs an image of a random Cyanide and Happiness comic. \
        Explosm makes awesome comics and animations."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mut tries = 0;
        let mut comic_url = String::new();
        let mut comic_img = None;
        let selector = scraper::Selector::parse(SELECTOR).unwrap();
        let client = UnicornHttpClient::new()?;
        while comic_img.is_none() && tries < 3 {
            comic_url = format!("https://explosm.net/comics/{}", Self::random());
            comic_img = Self::get_image(&client, &selector, &comic_url).await?;
            if comic_img.is_none() {
                tries += 1;
            }
        }
        let response = if let Some(comic_img) = comic_img {
            let author = CreateEmbedAuthor::new("Cyanide and Happiness")
                .icon_url(ICON)
                .url(comic_url);
            let embed = CreateEmbed::default()
                .color(0xff_66_00)
                .author(author)
                .image(&comic_img);
            CreateMessage::default().embed(embed)
        } else {
            UnicornEmbed::error("Failed to grab a comic, try again.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
