use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

use crate::common::InteractionCore;

const INTERACTION_ICON: &str = "👉";
const INTERACTION_COLOR: u64 = 0xff_cc_4d;

#[derive(Default)]
pub struct PokeCommand;

impl PokeCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for PokeCommand {
    fn command_name(&self) -> &str {
        "poke"
    }

    fn category(&self) -> &str {
        "interactions"
    }

    fn example(&self) -> &str {
        "@person"
    }

    fn description(&self) -> &str {
        "Poke, poke~ Are you alive?"
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let icore = InteractionCore::new(pld.db.clone(), self.command_name());
        let interaction = icore.get_one().await;
        let (actor, target) = InteractionCore::fetch_users(pld).await;
        let action = match target {
            Some(target) => format!("{} pokes {}.", actor.name, target.name),
            None => format!("{} pokes themself.", actor.name),
        };
        let response = interaction
            .embed(&pld.ctx, INTERACTION_COLOR, INTERACTION_ICON, action)
            .await;
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
