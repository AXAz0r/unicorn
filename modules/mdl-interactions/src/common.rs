use bson::doc;
use log::error;
use rand::Rng;
use serenity::model::user::User;

use unicorn_callable::payload::CommandPayload;
use unicorn_database::DatabaseHandler;

use crate::entry::InteractionEntry;
use serenity::futures::TryStreamExt;

pub struct InteractionCore {
    db: DatabaseHandler,
    name: String,
}

impl InteractionCore {
    pub fn new(db: DatabaseHandler, name: impl ToString) -> Self {
        Self {
            db,
            name: name.to_string(),
        }
    }

    fn cache_key(&self) -> String {
        format!("interactions:{}", &self.name)
    }

    async fn get_all_from_db(&self) -> Vec<InteractionEntry> {
        let mut items = Vec::<InteractionEntry>::new();
        if let Some(cli) = self.db.get_client() {
            let lookup = doc! {"name": &self.name, "active": true};
            if let Ok(cursor) = cli
                .database(&self.db.cfg.database.name)
                .collection::<bson::Document>("interactions")
                .find(lookup, None)
                .await
            {
                if let Ok(results) = cursor.try_collect::<Vec<bson::Document>>().await {
                    for doc in results {
                        if let Ok(inen) = bson::from_document::<InteractionEntry>(doc) {
                            items.push(inen);
                        }
                    }
                }
            }
        }
        items
    }

    async fn get_all_from_cache(&self) -> Option<Vec<InteractionEntry>> {
        let key = self.cache_key();
        let body = self.db.cache.get(key).await?;
        match serde_json::from_str::<Vec<InteractionEntry>>(&body) {
            Ok(items) => Some(items),
            Err(why) => {
                error!(
                    "Failed desererializing {} interactions cache: {}",
                    &self.name, why
                );
                None
            }
        }
    }

    async fn update_cache(&self, items: Vec<InteractionEntry>) {
        if !items.is_empty() {
            let key = self.cache_key();
            if let Ok(body) = InteractionEntry::vector_json(items) {
                self.db.cache.set(key, body).await;
            }
        }
    }

    pub async fn get_all(&self) -> Vec<InteractionEntry> {
        if let Some(items) = self.get_all_from_cache().await {
            items
        } else {
            let items = self.get_all_from_db().await;
            self.update_cache(items.clone()).await;
            items
        }
    }

    pub async fn get_one(&self) -> InteractionEntry {
        let mut items = self.get_all().await;
        if !items.is_empty() {
            let index = rand::thread_rng().gen_range(0..(items.len() - 1));
            let entry = items.remove(index);
            self.update_cache(items).await;
            entry
        } else {
            InteractionEntry::placeholder()
        }
    }

    pub async fn fetch_users(pld: &CommandPayload) -> (User, Option<User>) {
        let bot = pld.ctx.cache.current_user().clone();
        let mut actor = pld.msg.author.clone();
        let mut target = None;
        for mention in pld.msg.mentions.clone() {
            if mention.id.get() == bot.id.get() {
                actor = User::from(bot.clone());
            } else if target.is_none() {
                target = Some(mention);
            }
        }
        if actor.id == bot.id && target.is_none() {
            actor = pld.msg.author.clone();
            target = Some(User::from(bot));
        }
        (actor, target)
    }
}
