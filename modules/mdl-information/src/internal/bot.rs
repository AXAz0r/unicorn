// TODO: Add a link to the repository contributions page.

use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

const UNI_IMAGE: &str = "https://i.imgur.com/Ir25lus.png";
const SUPPORT_URL: &str = "https://discordapp.com/invite/aEUCHwX";

#[derive(Default)]
pub struct BotInfoCommand;

impl BotInfoCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn fetch_uni_version() -> String {
        let mut version = String::from("0.0.0");
        let text = include_str!("../../../../Cargo.toml");
        let lines = text.lines().collect::<Vec<&str>>();
        for line in lines {
            if line.starts_with("version") {
                let right = line.split('=').last().unwrap();
                version = right.replace("\"", "").trim().to_string();
                break;
            }
        }
        version
    }

    fn fetch_ser_version() -> String {
        let mut trigger = false;
        let lookup = "[dependencies.serenity]";
        let mut version = String::from("0.0.0");
        let text = include_str!("../../../../lib/unicorn-client/Cargo.toml");
        let lines = text.lines().collect::<Vec<&str>>();
        for line in lines {
            if line == lookup {
                trigger = true;
            }
            if line.starts_with("version") && trigger {
                let right = line.split('=').last().unwrap();
                version = right.replace("\"", "").trim().to_string();
                break;
            }
        }
        version
    }
}

#[serenity::async_trait]
impl UnicornCommand for BotInfoCommand {
    fn command_name(&self) -> &str {
        "botinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["botinfo", "info"]
    }

    fn description(&self) -> &str {
        "Shows information about the bot, version, codename, authors, etc."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let uni_version = Self::fetch_uni_version();
        let ser_version = Self::fetch_ser_version();
        let author = CreateEmbedAuthor::new(format!("Apex Sigma: v{} Unicorn", uni_version))
            .icon_url(UNI_IMAGE)
            .url(SUPPORT_URL);
        let embed = CreateEmbed::default()
            .color(0x6e_6b_a6)
            .author(author)
            .field("Authors", "<Repository contributors.>", true)
            .field(
                "Environment",
                format!(
                    "Language: **Rust**\nLibrary: **Serenity v{}**\nPlatform: **{}**",
                    ser_version,
                    std::env::consts::OS.to_uppercase()
                ),
                true,
            );
        let response = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
