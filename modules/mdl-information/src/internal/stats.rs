use serenity::builder::{CreateEmbed, CreateEmbedFooter, CreateMessage};
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

pub struct BotStatsCommand {
    stamp: i64,
}

impl Default for BotStatsCommand {
    fn default() -> Self {
        Self {
            stamp: chrono::Utc::now().timestamp(),
        }
    }
}

impl BotStatsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self::default())
    }
}

#[serenity::async_trait]
impl UnicornCommand for BotStatsCommand {
    fn command_name(&self) -> &str {
        "statistics"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["stats"]
    }

    fn description(&self) -> &str {
        "Shows Sigma's current statistics. \
        Population, message and command counts, and rates since startup."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let cache = pld.ctx.cache.clone();
        let guild_count = pld.ctx.cache.guild_count();
        let mut role_count = 0u64;
        let mut user_count = 0u64;
        let channel_count = cache.guild_channel_count();
        for gid in &cache.guilds() {
            if let Some(guild) = cache.guild(gid) {
                role_count += guild.roles.len() as u64;
                user_count += guild.member_count;
            }
        }
        let now = chrono::Utc::now().timestamp();
        let footer = CreateEmbedFooter::new(format!(
            "Tracking since {} UTC ({}s ago).",
            chrono::DateTime::<chrono::Utc>::from_timestamp(self.stamp, 0)
                .unwrap()
                .format("%e. %B %Y %T"),
            now - self.stamp
        ));
        let embed = CreateEmbed::default()
            .color(0xf9_f9_f9)
            .title("📊 Instance Statistics")
            .field(
                "Population",
                format!(
                    "Guilds: **{}**\nChannels: **{}**\nRoles: **{}**\nUsers: **{}**",
                    guild_count, channel_count, role_count, user_count
                ),
                true,
            )
            .field(
                "Usage",
                format!(
                    "Commands: **{}**\nCommand Rate: **{}**/s\nMessages: **{}**/s\nMessage Rate: **{}**",
                    "Unknown", "Unknown", "Unknown", "Unknown"
                ),
                true,
            )
            .footer(footer);
        let message = CreateMessage::default().embed(embed);
        let _ = pld
            .msg
            .channel_id
            .send_message(&pld.ctx.http, message)
            .await;
        Ok(())
    }
}
