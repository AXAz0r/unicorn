use serenity::builder::{CreateEmbed, CreateMessage};

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

#[derive(Default)]
pub struct BotOwnersCommand;

impl BotOwnersCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for BotOwnersCommand {
    fn command_name(&self) -> &str {
        "owners"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn description(&self) -> &str {
        "Shows a list of Sigma's owners. \
        Users in this list have access to the administration module."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mut lines = Vec::new();
        for oid in &pld.cfg.discord.owners {
            if let Some(owner) = pld.ctx.cache.user(*oid) {
                lines.push(format!(
                    "{}#{:0>4}",
                    owner.name,
                    if let Some(disc) = owner.discriminator {
                        disc.get()
                    } else {
                        0
                    }
                ))
            } else {
                lines.push(oid.to_string());
            }
        }
        let embed = CreateEmbed::default()
            .color(0x3b_88_c3)
            .title("🛡️ Bot Owners")
            .description(lines.join(", "));
        let response = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
