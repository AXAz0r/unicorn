use serenity::model::guild::Guild;
use serenity::model::id::ChannelId;

use unicorn_callable::payload::CommandPayload;

pub struct ChannelUtils;

impl ChannelUtils {
    pub async fn search_guild(pld: &CommandPayload, guild: Guild, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        if let Ok(looknum) = lookup.parse::<u64>() {
            guild.channels.get(&ChannelId::new(looknum)).map(|chn| chn.clone().id)
        } else {
            for cid in guild.channels.keys() {
                let channel_name = cid.name(&pld.ctx).await.unwrap_or_default();
                let lower_match = channel_name.to_lowercase() == lookup.to_lowercase();
                let pound_match =
                    channel_name.to_lowercase() == format!("#{}", lookup.to_lowercase());
                if lower_match || pound_match {
                    best = Some(*cid);
                    break;
                }
            }
            if best.is_none() {
                for cid in guild.channels.keys() {
                    let channel_name = cid.name(&pld.ctx).await.unwrap_or_default();
                    let lower_cont = channel_name.to_lowercase().contains(&lookup.to_lowercase());
                    let pound_cont = channel_name
                        .to_lowercase()
                        .contains(&format!("#{}", lookup.to_lowercase()));
                    if lower_cont || pound_cont {
                        best = Some(*cid);
                        break;
                    }
                }
            }
            best
        }
    }

    pub async fn search_local(pld: &CommandPayload, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
        if let Some(guild) = gld {
            let guild = guild.clone();
            best = Self::search_guild(pld, guild, lookup).await
        }
        best
    }

    pub async fn mentioned_channels(pld: &CommandPayload) -> Vec<ChannelId> {
        let is_owner = pld.cfg.discord.owners.contains(&pld.msg.author.id.get());
        let mut mentions = Vec::<ChannelId>::new();
        if let Some(guild) = pld.msg.guild(&pld.ctx.cache) {
            for arg in &pld.args.raw() {
                if arg.starts_with("<#") && arg.ends_with('>') {
                    if let Ok(cid) = arg
                        .trim_start_matches("<#")
                        .trim_end_matches('>')
                        .parse::<u64>()
                    {
                        let channel_id = ChannelId::new(cid);
                        if guild.channels.contains_key(&channel_id) || is_owner {
                            mentions.push(channel_id);
                        }
                    }
                }
            }
        }
        mentions
    }
}
