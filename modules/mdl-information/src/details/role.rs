use serenity::all::CreateEmbedAuthor;
use serenity::builder::{CreateEmbed, CreateMessage};
use serenity::model::guild::{Guild, Role};
use serenity::model::id::RoleId;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::shorts::UnicornShorts;

#[derive(Default)]
pub struct RoleInfoCommand;

impl RoleInfoCommand {
    fn count_members(guild: &Guild, role: &Role) -> u64 {
        let mut counter = 0;
        for member in guild.members.values() {
            for mrole in &member.roles {
                if mrole.get() == role.id.get() {
                    counter += 1;
                    break;
                }
            }
        }
        counter
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn describe(guild: &Guild, role: &Role) -> String {
        format!(
            "Name: **{}**\n\
             ID: **{}**\n\
             Color: **#{}**\n\
             Hoisted: **{}**\n\
             Members: **{}**\n\
             Created: **{}**",
            role.name,
            role.id.get(),
            role.colour.hex(),
            if role.hoist { "Yes" } else { "No" },
            if guild.id.get() == role.id.get() {
                guild.member_count
            } else {
                Self::count_members(guild, role)
            },
            UnicornShorts::format_timestamp(role.id.created_at())
        )
    }

    pub async fn find_role(pld: &CommandPayload) -> (bool, Option<RoleId>) {
        let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
        if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            if lookup.starts_with("<@&") && lookup.ends_with('>') {
                if let Ok(rid_num) = lookup
                    .trim_start_matches("<@&")
                    .trim_end_matches('>')
                    .parse::<u64>()
                {
                    (false, Some(RoleId::from(rid_num)))
                } else {
                    (false, None)
                }
            } else {
                let mut best = None;
                let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
                if let Some(guild) = gld {
                    for (rid, role) in &guild.roles {
                        if role.name.to_lowercase() == lookup.to_lowercase()
                            || rid.to_string() == lookup
                        {
                            best = Some(*rid);
                            break;
                        }
                    }
                    if best.is_none() {
                        for (rid, role) in &guild.roles {
                            if role.name.to_lowercase().contains(&lookup.to_lowercase()) {
                                best = Some(*rid);
                                break;
                            }
                        }
                    }
                }
                (false, best)
            }
        } else if let Some(guild) = gld {
            if let Ok(member) = guild.member(&pld.ctx.http, pld.msg.author.id).await {
                let member = member.clone();
                if let Some(top_role) = guild.member_highest_role(&member) {
                    (true, Some(top_role.clone().id))
                } else {
                    (true, None)
                }
            } else {
                (true, None)
            }
        } else {
            (true, None)
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for RoleInfoCommand {
    fn command_name(&self) -> &str {
        "roleinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["roleinfo", "rinfo"]
    }

    fn example(&self) -> &str {
        "Bouncer"
    }

    fn description(&self) -> &str {
        "Shows information and data on the specified role. \
         Roles mentions do not work here, \
         lookup is done via role name or role mention. \
         If no lookup is given, it will show information about the author's highest role."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let (no_lookup, rid) = Self::find_role(pld).await;
        let response = if let Some(rid) = rid {
            if let Some(guild) = pld.msg.guild(&pld.ctx.cache) {
                if let Some(role) = guild.roles.get(&rid) {
                    let mut ath = CreateEmbedAuthor::new(format!("{} Information", &role.name));
                    if let Some(icon) = guild.icon_url() {
                        ath = ath.icon_url(icon);
                    }
                    let emb = CreateEmbed::default()
                        .author(ath)
                        .color(role.colour)
                        .description(Self::describe(&guild, role));
                    CreateMessage::default().embed(emb)
                } else {
                    UnicornEmbed::error("Failed getting the role from the guild's cache.")
                }
            } else {
                UnicornEmbed::error("Failed to get guild information.")
            }
        } else if no_lookup {
            UnicornEmbed::error("You don't have any roles.")
        } else {
            UnicornEmbed::not_found("Role not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
