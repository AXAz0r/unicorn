use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateEmbedFooter, CreateMessage};
use serenity::model::guild::Member;
use serenity::model::id::UserId;
use serenity::model::user::User;
use serenity::model::Colour;
use std::collections::HashMap;
use std::ops::Deref;
use unicorn_callable::argument::CommandArgument;

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::shorts::UnicornShorts;
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct UserInfoCommand;

impl UserInfoCommand {
    pub async fn user_to_member(pld: &CommandPayload, user: User) -> Option<Member> {
        let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
        if let Some(guild) = gld {
            if let Ok(member) = guild.member(&pld.ctx.http, user.id).await {
                Some(member.deref().clone())
            } else {
                None
            }
        } else {
            None
        }
    }

    fn best_member(members: HashMap<UserId, Member>, lookup: &str) -> Option<Member> {
        let mut best = None;
        for member in members.values() {
            let user = &member.user.clone();
            let full = format!(
                "{}#{:0>4}",
                &user.name,
                if let Some(disc) = user.discriminator {
                    disc.get()
                } else {
                    0
                }
            );
            if full.to_lowercase() == lookup.to_lowercase()
                || user.name.to_lowercase() == lookup.to_lowercase()
            {
                best = Some(member.clone());
            } else if let Some(nick) = &member.nick {
                if nick.to_lowercase() == lookup.to_lowercase() {
                    best = Some(member.clone());
                }
            }
            if best.is_some() {
                break;
            }
        }
        if best.is_none() {
            for member in members.values() {
                let user = &member.user.clone();
                if user.name.to_lowercase().contains(&lookup.to_lowercase()) {
                    best = Some(member.clone());
                } else if let Some(nick) = &member.nick {
                    if nick.to_lowercase().contains(&lookup.to_lowercase()) {
                        best = Some(member.clone());
                    }
                } else if user.id.to_string() == lookup {
                    best = Some(member.clone());
                }
                if best.is_some() {
                    break;
                }
            }
        }
        best
    }

    async fn search_local(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
        if let Some(guild) = gld {
            if let Ok(nl) = lookup.parse::<u64>() {
                if let Ok(memb) = guild.member(&pld.ctx.http, nl).await {
                    Some(memb.deref().clone())
                } else {
                    None
                }
            } else {
                Self::best_member(guild.members.clone(), lookup)
            }
        } else {
            None
        }
    }

    async fn search_global(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        let mut best = None;
        let lookup_int = lookup.parse::<u64>();
        for gid in pld.ctx.cache.guilds() {
            let gld = pld.ctx.cache.guild(gid).map(|gld| gld.clone());
            if let Some(guild) = gld {
                if let Ok(nl) = lookup_int {
                    if let Ok(memb) = guild.member(&pld.ctx.http, nl).await {
                        best = Some(memb.deref().clone());
                    }
                }
                if best.is_none() {
                    let members = guild.members.clone();
                    best = Self::best_member(members, lookup);
                    if best.is_some() {
                        break;
                    }
                }
            }
        }
        best
    }

    pub async fn search(pld: &CommandPayload, lookup: &str) -> Option<Member> {
        if let Some(member) = Self::search_local(pld, lookup).await {
            Some(member)
        } else if pld.cfg.discord.owners.contains(&pld.msg.author.id.get()) {
            Self::search_global(pld, lookup).await
        } else {
            None
        }
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UserInfoCommand {
    fn command_name(&self) -> &str {
        "userinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["userinfo", "uinfo"]
    }

    fn example(&self) -> &str {
        "@target"
    }

    fn description(&self) -> &str {
        "Shows information and data on the mentioned user. \
         If no user is mentioned, it will show data for the author."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("target", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let target = if pld.msg.mentions.is_empty() {
            if pld.args.has_argument("target") {
                Self::search(pld, pld.args.get("target")).await
            } else {
                Self::user_to_member(pld, pld.msg.author.clone()).await
            }
        } else {
            Self::user_to_member(pld, pld.msg.mentions[0].clone()).await
        };
        let response = if let Some(target) = target {
            let user = target.user.clone();
            let color_option = target.colour(&pld.ctx.cache);
            let avatar = Avatar::url(target.user.clone());
            let author = CreateEmbedAuthor::new(format!("{}'s Information", target.display_name()))
                .icon_url(&avatar)
                .url(avatar);
            let footer = CreateEmbedFooter::new(format!(
                "Use the {}avatar command to see the user's avatar, or click the title.",
                pld.get_prefix()
            ));
            let guild = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());

            let embed = CreateEmbed::default()
                .author(author)
                .footer(footer)
                .color(color_option.unwrap_or_else(|| Colour::from(0)))
                .field(
                    "User Info",
                    format!(
                        "Username: **{}**#{:0>4}\n\
                         ID: **{}**\n\
                         Bot User: **{}**\n\
                         Created: **{}**",
                        &user.name,
                        if let Some(disc) = user.discriminator {
                            disc.get()
                        } else {
                            0
                        },
                        user.id.get(),
                        if user.bot { "True" } else { "False" },
                        UnicornShorts::format_timestamp(user.created_at()),
                    ),
                    true,
                )
                .field(
                    "Member Info",
                    format!(
                        "Nickname: **{}**\n\
                         Color: **#{}**\n\
                         Top Role: **{}**\n\
                         Joined: **{}**",
                        if let Some(nick) = &target.nick {
                            nick
                        } else {
                            &user.name
                        },
                        if let Some(color) = color_option {
                            color.hex()
                        } else {
                            "000000".to_owned()
                        },
                        if let Some(guild) = guild {
                            if let Some(role) = guild.member_highest_role(&target) {
                                role.name.clone()
                            } else {
                                "everyone".to_owned()
                            }
                        } else {
                            "everyone".to_owned()
                        },
                        if let Some(joined_at) = target.joined_at {
                            UnicornShorts::format_timestamp(joined_at)
                        } else {
                            "Unknown".to_owned()
                        },
                    ),
                    true,
                );
            CreateMessage::default().embed(embed)
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
