use serenity::all::AfkTimeout;
use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use serenity::model::guild::{Guild, Member, VerificationLevel};
use serenity::model::id::{GuildId, UserId};
use std::collections::HashMap;

use unicorn_callable::argument::CommandArgument;

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::shorts::UnicornShorts;

#[derive(Default)]
pub struct GuildInfoCommand;

impl GuildInfoCommand {
    fn count_bots(members: &HashMap<UserId, Member>) -> u64 {
        let mut bots = 0;
        for memb in members.values() {
            let usr = memb.user.clone();
            if usr.bot {
                bots += 1;
            }
        }
        bots
    }

    async fn guild_info(gld: &Guild) -> String {
        let bots = Self::count_bots(&gld.members);
        let (_cats, texts, voices) = ("?", "?", "?");
        format!(
            "Name: **{}**\n\
            ID: **{}**\n\
            Roles: **{}**\n\
            Members: **{}** (+{} Bots)\n\
            Channels: **{}** Text; **{}** Voice
            Created: **{}**",
            &gld.name,
            gld.id.get(),
            gld.roles.len(),
            gld.member_count - bots,
            bots,
            texts,
            voices,
            UnicornShorts::format_timestamp(gld.id.created_at())
        )
    }

    async fn owner_info(pld: &CommandPayload, gld: &Guild, own: &Member) -> String {
        let usr = own.user.clone();
        format!(
            "Name: **{}**#{:0>4}\n\
             Nickname: **{}**\n\
             ID: **{}**\n\
             Color: **#{}**\n\
             Top Role: **{}**\n\
             Created: **{}**\n",
            &usr.name,
            if let Some(disc) = &usr.discriminator {
                disc.get()
            } else {
                0
            },
            if let Some(nick) = &own.nick {
                nick
            } else {
                &usr.name
            },
            &usr.id.get(),
            if let Some(color) = own.colour(&pld.ctx.cache) {
                color.hex()
            } else {
                "000000".to_owned()
            },
            if let Some(role) = gld.member_highest_role(own) {
                role.name.clone()
            } else {
                "everyone".to_owned()
            },
            UnicornShorts::format_timestamp(usr.created_at())
        )
    }

    async fn details(pld: &CommandPayload, gld: &Guild) -> String {
        format!(
            "AFK: **{}** ({}s)\n\
             Emojis: **{}**\n\
             Large: **{}**\n\
             Shard: **{}**\n\
             Verification: **{}**",
            if let Some(afk_md) = &gld.afk_metadata {
                let afk_cid = afk_md.afk_channel_id;
                afk_cid
                    .name(&pld.ctx)
                    .await
                    .unwrap_or_else(|_| "None".to_owned())
            } else {
                "None".to_owned()
            },
            if let Some(afk_md) = &gld.afk_metadata {
                match afk_md.afk_timeout {
                    AfkTimeout::OneMinute => 60.to_string(),
                    AfkTimeout::FiveMinutes => 300.to_string(),
                    AfkTimeout::FifteenMinutes => 900.to_string(),
                    AfkTimeout::ThirtyMinutes => 1800.to_string(),
                    AfkTimeout::OneHour => 3600.to_string(),
                    AfkTimeout::Unknown(val) => val.to_string(),
                    _ => "Unknown".to_string(),
                }
            } else {
                "None".to_owned()
            },
            gld.emojis.len(),
            if gld.large { "Yes" } else { "No" },
            gld.shard_id(&pld.ctx.cache),
            match gld.verification_level {
                VerificationLevel::Low => "Low",
                VerificationLevel::Medium => "Medium",
                VerificationLevel::High => "High",
                VerificationLevel::Higher => "Extreme",
                _ => "None",
            }
        )
    }

    async fn search(pld: &CommandPayload, lookup: impl ToString) -> Option<GuildId> {
        let mut best = None;
        let lookup = lookup.to_string();
        let guilds = pld.ctx.cache.guilds();
        for gid in &guilds {
            if let Some(guild) = pld.ctx.cache.guild(gid) {
                if guild.name.to_lowercase() == lookup.to_lowercase() {
                    best = Some(*gid);
                    break;
                }
            }
        }
        if best.is_none() {
            for gid in &guilds {
                if let Some(guild) = pld.ctx.cache.guild(gid) {
                    if guild.name.to_lowercase().contains(&lookup.to_lowercase()) {
                        best = Some(*gid);
                        break;
                    }
                }
            }
        }
        best
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GuildInfoCommand {
    fn command_name(&self) -> &str {
        "serverinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["srvinfo", "sinfo", "guildinformation", "guildinfo", "ginfo"]
    }

    fn description(&self) -> &str {
        "Shows information about the guild/server you execute this command on."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", false).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let guild = if pld.args.has_argument("lookup")
            && pld.cfg.discord.owners.contains(&pld.msg.author.id.get())
        {
            if let Ok(gid) = pld.args.get("lookup").parse::<u64>() {
                pld.ctx.cache.guild(gid).map(|gld| gld.clone())
            } else if let Some(gid) = Self::search(pld, pld.args.get("lookup")).await {
                pld.ctx.cache.guild(gid).map(|gld| gld.clone())
            } else {
                None
            }
        } else {
            pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone())
        };
        let response = if let Some(gld) = guild {
            let (icon, color) = if let Some(icon) = gld.icon_url() {
                (icon.clone(), ImageProcessing::from_url(icon.clone()).await)
            } else {
                ("https://i.imgur.com/QnYSlld.png".to_owned(), 0x1b_6f_5f)
            };
            let owner_option = gld.member(&pld.ctx.http, &gld.owner_id).await;
            let owner_info = if let Ok(own) = owner_option {
                Self::owner_info(pld, &gld, &own).await
            } else {
                "Failed to retrieve owner information.".to_owned()
            };
            let details = Self::details(pld, &gld).await;
            let guild_info = Self::guild_info(&gld).await;
            let author = CreateEmbedAuthor::new(&gld.name).icon_url(&icon).url(&icon);
            let embed = CreateEmbed::default()
                .color(color)
                .author(author)
                .field("Guild Info", guild_info, true)
                .field("Owner Info", owner_info, true)
                .field("Details", details, true);
            CreateMessage::default().embed(embed)
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
