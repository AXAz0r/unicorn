use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use serenity::model::channel::{ChannelType, GuildChannel};
use serenity::model::id::ChannelId;

use unicorn_callable::argument::CommandArgument;

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::shorts::UnicornShorts;

use crate::utils::ChannelUtils;

#[derive(Default)]
pub struct ChannelInfoCommand;

impl ChannelInfoCommand {
    async fn search_global(pld: &CommandPayload, lookup: &str) -> Option<ChannelId> {
        let mut best = None;
        let guilds = pld.ctx.cache.guilds();
        for gid in &guilds {
            let gld = pld.ctx.cache.guild(gid).map(|gld| gld.clone());
            if let Some(guild) = gld {
                best = ChannelUtils::search_guild(pld, guild.clone(), lookup).await;
                if best.is_some() {
                    break;
                }
            }
        }
        best
    }

    async fn search(pld: &CommandPayload, lookup: impl ToString) -> Option<ChannelId> {
        let lookup = lookup.to_string();
        if let Some(cid) = ChannelUtils::search_local(pld, &lookup).await {
            Some(cid)
        } else if pld.cfg.discord.owners.contains(&pld.msg.author.id.get()) {
            Self::search_global(pld, &lookup).await
        } else {
            None
        }
    }

    fn describe(chn: &GuildChannel) -> String {
        format!(
            "Name: **{}**\n\
             ID: **{}**\n\
             Type: **{}**\n\
             Position: **{}**\n\
             NSFW: **{}**\n\
             Created: **{}**",
            match chn.kind {
                ChannelType::Text => format!("#{}", chn.name),
                _ => chn.name.clone(),
            },
            chn.id.get(),
            match chn.kind {
                ChannelType::Text => "Text Channel".to_owned(),
                ChannelType::Voice => "Voice Channel".to_owned(),
                _ => "Weird Channel".to_owned(),
            },
            chn.position,
            if chn.nsfw { "Yes" } else { "No" },
            UnicornShorts::format_timestamp(chn.id.created_at()),
        )
    }

    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ChannelInfoCommand {
    fn command_name(&self) -> &str {
        "channelinformation"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["chinfo", "cinfo", "chinformation", "channelinfo"]
    }

    fn description(&self) -> &str {
        "Shows information about the mentioned channel. \
         If you don't tag a channel, it will target the channel that \
         the the command was executed in."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("channel", true)
                .available_when(|s| s.starts_with("<#") && s.ends_with('>'))
                .anywhere(),
            CommandArgument::new("lookup", false).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mentions = ChannelUtils::mentioned_channels(pld).await;
        let cid = if !mentions.is_empty() {
            Some(mentions[0])
        } else if pld.args.has_argument("lookup") {
            let lookup = pld.args.get("lookup");
            Self::search(pld, lookup).await
        } else {
            Some(pld.msg.channel_id)
        };
        let response = if let Some(cid) = cid {
            if let Ok(chn) = cid.to_channel(&pld.ctx.http).await {
                let is_local = if let Some(gld) = pld.msg.guild(&pld.ctx.cache) {
                    gld.channels.contains_key(&cid)
                } else {
                    false
                };
                if let Some(chn) = chn.guild() {
                    let guild_option = chn.guild(&pld.ctx.cache).map(|gld| gld.clone());
                    let (name, color, icon) = if let Some(guild) = guild_option {
                        let guild = guild.clone();
                        (
                            Some(if is_local {
                                format!(
                                    "{}: {}",
                                    guild.name.clone(),
                                    match chn.kind {
                                        ChannelType::Text => format!("#{}", &chn.name),
                                        _ => chn.name.clone(),
                                    }
                                )
                            } else {
                                format!(
                                    "{}: {} (External)",
                                    &guild.name,
                                    match chn.kind {
                                        ChannelType::Text => format!("#{}", &chn.name),
                                        _ => chn.name.clone(),
                                    }
                                )
                            }),
                            if let Some(icon) = guild.icon_url() {
                                Some(ImageProcessing::from_url(icon).await)
                            } else {
                                None
                            },
                            guild.icon_url(),
                        )
                    } else {
                        (None, None, None)
                    };
                    let mut author = None;
                    if let Some(name) = name {
                        let mut pauthor = CreateEmbedAuthor::new(name);
                        if let Some(icon) = icon {
                            pauthor = pauthor.icon_url(icon);
                        }
                        author = Some(pauthor)
                    }
                    let mut embed = CreateEmbed::default()
                        .description(Self::describe(&chn))
                        .color(color.unwrap_or(0x1b_6f_5f));
                    if let Some(author) = author {
                        embed = embed.author(author)
                    }
                    CreateMessage::default().embed(embed)
                } else {
                    UnicornEmbed::error("Missing channel guild entry!")
                }
            } else {
                UnicornEmbed::error("Missing channel cache entry!")
            }
        } else {
            UnicornEmbed::not_found("Channel not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
