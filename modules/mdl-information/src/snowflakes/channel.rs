use serenity::builder::{CreateEmbed, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::utils::ChannelUtils;

#[derive(Default)]
pub struct ChannelIDCommand;

impl ChannelIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ChannelIDCommand {
    fn command_name(&self) -> &str {
        "channelid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["chnid", "chid", "cid"]
    }

    fn description(&self) -> &str {
        "Shows the tagged channel's snowflake (ID). \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device. \
         If you do not tag a channel or enter a lookup, the command with target the channel \
         that it is executed in."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("--text", true).flag(),
            CommandArgument::new("lookup", true).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mentioned = ChannelUtils::mentioned_channels(pld).await;
        let text = pld.args.has_argument("--text");
        let target = if !mentioned.is_empty() {
            Some(mentioned[0])
        } else if pld.args.has_argument("lookup") {
            ChannelUtils::search_local(pld, pld.args.get("lookup")).await
        } else {
            Some(pld.msg.channel_id)
        };
        let target = if let Some(cid) = target {
            let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
            if let Some(guild) = gld {
                guild.channels.get(&cid).cloned()
            } else {
                None
            }
        } else {
            None
        };
        let response = if let Some(target) = target {
            let mut msg = CreateMessage::default();
            let target_name = &target.name;
            if text {
                msg = msg.content(target.id.get().to_string());
            } else {
                let embed = CreateEmbed::default()
                    .title(format!("#️ #{}", target_name))
                    .description(target.id.get().to_string())
                    .colour(0x3b_88_c3);
                msg = msg.embed(embed);
            }
            msg
        } else {
            UnicornEmbed::not_found("Channel not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
