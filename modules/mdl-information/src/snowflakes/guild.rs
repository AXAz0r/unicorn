use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct GuildIDCommand;

impl GuildIDCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GuildIDCommand {
    fn command_name(&self) -> &str {
        "guildid"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["gldid", "gid", "serverid", "srvid", "sid"]
    }

    fn description(&self) -> &str {
        "Shows the current guild's snowflake (ID). \
         Add the \"--text\" parameter to the command to return it as a \
         regular text messages instead of an embed. Useful if you're on a mobile device."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("--text", true).flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mut response = CreateMessage::default();
        let text = pld.args.has_argument("--text");
        let gid = if let Some(gid) = pld.msg.guild_id {
            gid.get().to_string()
        } else {
            "Unknown".to_owned()
        };
        if text {
            response = response.content(gid);
        } else {
            let embed = CreateEmbed::default()
                .title(format!("🔢 {}", gid))
                .color(0x3b_88_c3);
            response = response.embed(embed);
        }
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
