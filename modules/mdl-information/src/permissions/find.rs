use serenity::all::CreateEmbedFooter;
use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use serenity::model::Permissions;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;

#[derive(Default)]
pub struct FindCommand;

impl FindCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn perm_from_name(pld: &CommandPayload) -> Option<Permissions> {
        let mut perm = None;
        for i in 0..39 {
            if let Some(p) = Permissions::from_bits(1 << i) {
                let pnames = p.get_permission_names();
                if !pnames.is_empty()
                    && pnames[0].to_lowercase() == pld.args.get("lookup").to_lowercase()
                {
                    perm = Some(p);
                    break;
                }
            }
        }
        perm
    }
}

#[serenity::async_trait]
impl UnicornCommand for FindCommand {
    fn command_name(&self) -> &str {
        "roleswithpermission"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["rolewithperm", "rwperm", "rwp"]
    }

    fn description(&self) -> &str {
        "Shows all roles that have the specified permission."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if pld.args.has_argument("lookup") {
            if let Some(perm) = Self::perm_from_name(pld) {
                let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
                if let Some(guild) = gld {
                    let (icon, color) = if let Some(icon) = guild.icon_url() {
                        (icon.clone(), ImageProcessing::from_url(icon.clone()).await)
                    } else {
                        ("https://i.imgur.com/QnYSlld.png".to_owned(), 0x1b_6f_5f)
                    };
                    let mut role_names = Vec::<String>::new();
                    for role in guild.roles.values() {
                        if role.has_permission(perm) {
                            let name = match role.managed {
                                true => format!("{}*", role.name),
                                false => role.name.clone(),
                            };
                            role_names.push(name)
                        }
                    }
                    if !role_names.is_empty() {
                        let names = role_names.join(", ").replace('@', "@\u{200B}"); // zero-width space
                        let author = CreateEmbedAuthor::new(&guild.name).icon_url(&icon);
                        let mut embed = CreateEmbed::default()
                            .color(color)
                            .author(author)
                            .description(names.clone());
                        if names.contains('*') {
                            let footer = CreateEmbedFooter::new("Roles with an asterisk are managed by an integration (most likely a bot)");
                            embed = embed.footer(footer);
                        }
                        CreateMessage::default().embed(embed)
                    } else {
                        UnicornEmbed::not_found("No roles have that permission.")
                    }
                } else {
                    UnicornEmbed::error("Failed to get guild information.")
                }
            } else {
                let perm_names = Permissions::all().get_permission_names();
                let desc = format!("The available permissions are: {}", perm_names.join(", "));
                let embed = CreateEmbed::default()
                    .color(0x69_69_69)
                    .title("🔍 No roles have that permission.")
                    .description(desc);
                CreateMessage::default().embed(embed)
            }
        } else {
            UnicornEmbed::error("No lookup given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
