use serenity::builder::{CreateEmbed, CreateMessage};
use serenity::model::Permissions;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

use crate::details::role::RoleInfoCommand;

#[derive(Default)]
pub struct RolePermsCommand;

impl RolePermsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for RolePermsCommand {
    fn command_name(&self) -> &str {
        "rolepermissions"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["roleperms", "rperms"]
    }

    fn description(&self) -> &str {
        "Shows the specified role's permissions. If no role is specified, it will show permissions for \
        the author's top role. To avoid pinging users in a mentionable role, use the role name or ID instead."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("lookup", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let (no_lookup, rid) = RoleInfoCommand::find_role(pld).await;
        let response = if let Some(rid) = rid {
            let gld = pld.msg.guild(&pld.ctx.cache).map(|gld| gld.clone());
            if let Some(guild) = gld {
                if let Some(role) = guild.roles.get(&rid) {
                    let mut perms = role.permissions;
                    if perms.administrator() {
                        perms = Permissions::all()
                    }
                    let perm_names = perms.get_permission_names();
                    let mut perms_block = "```\n".to_owned();
                    for perm in perm_names {
                        perms_block.push_str(&format!("- {}\n", perm));
                    }
                    perms_block.push_str("```");
                    let embed = CreateEmbed::default()
                        .title(format!("{} Permissions", role.name))
                        .color(role.colour)
                        .description(perms_block);
                    CreateMessage::default().embed(embed)
                } else {
                    UnicornEmbed::error("Failed getting the role from the guild's cache.")
                }
            } else {
                UnicornEmbed::error("Failed to get guild information.")
            }
        } else if no_lookup {
            UnicornEmbed::error("You don't have any roles.")
        } else {
            UnicornEmbed::not_found("Role not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
