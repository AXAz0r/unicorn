use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::user::Avatar;

use crate::details::user::UserInfoCommand;

#[derive(Default)]
pub struct UserPermsCommand;

impl UserPermsCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for UserPermsCommand {
    fn command_name(&self) -> &str {
        "userpermissions"
    }

    fn category(&self) -> &str {
        "information"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["userperms", "uperms"]
    }

    fn description(&self) -> &str {
        "Shows the mentioned user's permissions. If no user is mentioned, it will show permissions for the author."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("target", true).allows_spaces()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let target = if pld.msg.mentions.is_empty() {
            if pld.args.has_argument("target") {
                UserInfoCommand::search(pld, pld.args.get("target")).await
            } else {
                UserInfoCommand::user_to_member(pld, pld.msg.author.clone()).await
            }
        } else {
            UserInfoCommand::user_to_member(pld, pld.msg.mentions[0].clone()).await
        };

        let response = if let Some(target) = target {
            if let Some(perms) = target.permissions {
                let user = target.user.clone();
                let color = ImageProcessing::from_url(Avatar::url(user.clone())).await;
                let perm_names = perms.get_permission_names();
                let mut perms_block = "```\n".to_owned();
                for perm in perm_names {
                    perms_block.push_str(&format!("- {}\n", perm));
                }
                perms_block.push_str("```");
                let author = CreateEmbedAuthor::new(format!("{}'s Permissions", user.name.clone()));
                let embed = CreateEmbed::default()
                    .author(author)
                    .color(color)
                    .description(perms_block);
                CreateMessage::default().embed(embed)
            } else {
                UnicornEmbed::error("Failed to retrieve member permissions.")
            }
        } else {
            UnicornEmbed::not_found("User not found.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
