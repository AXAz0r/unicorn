use unicorn_callable::module::UnicornModule;
use unicorn_callable::{UnicornCommand, UnicornEvent};

use crate::{
    details::{
        channel::ChannelInfoCommand,
        guild::GuildInfoCommand,
        role::RoleInfoCommand,
        user::UserInfoCommand,
    },
    internal::{
        bot::BotInfoCommand,
        owners::BotOwnersCommand,
        stats::BotStatsCommand,
    },
    permissions::{
        find::FindCommand,
        role::RolePermsCommand,
        user::UserPermsCommand,
    },
    snowflakes::{
        channel::ChannelIDCommand,
        guild::GuildIDCommand,
        role::RoleIDCommand,
        user::UserIDCommand,
    },
};

mod details;
mod internal;
mod permissions;
mod snowflakes;
mod utils;

#[derive(Default)]
pub struct InformationModule;

impl InformationModule {
    pub fn boxed() -> Box<Self> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornModule for InformationModule {
    fn commands(&self) -> Vec<Box<dyn UnicornCommand>> {
        vec![
            // Details
            UserInfoCommand::boxed(),
            GuildInfoCommand::boxed(),
            ChannelInfoCommand::boxed(),
            RoleInfoCommand::boxed(),
            // Internal
            BotInfoCommand::boxed(),
            BotOwnersCommand::boxed(),
            BotStatsCommand::boxed(),
            // Permissions
            FindCommand::boxed(),
            RolePermsCommand::boxed(),
            UserPermsCommand::boxed(),
            // Snowflakes
            UserIDCommand::boxed(),
            GuildIDCommand::boxed(),
            ChannelIDCommand::boxed(),
            RoleIDCommand::boxed(),
        ]
    }

    fn events(&self) -> Vec<Box<dyn UnicornEvent>> {
        Vec::new()
    }

    fn name(&self) -> &str {
        "Information"
    }
}
