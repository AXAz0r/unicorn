# Information Module

Inherits a lot of commands from the `utility` module.

## Commands

### Details

- [x] Channel
- [x] Guild
- [x] Role
- [x] User

### Internal

- [x] Bot Info
- [x] Owners
- [ ] Statistics

### Permissions

- [ ] Find
- [ ] Role
- [ ] User

### Snowflakes

- [x] Channel
- [x] Guild
- [x] Role
- [x] User

## Events

TODO!
