use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use serenity::model::user::CurrentUser;
use serenity::model::user::User;

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;
use unicorn_callable::payload::CommandPayload;
use unicorn_utility::image::ImageProcessing;
use unicorn_utility::user::Avatar;

#[derive(Default)]
pub struct InviteCommand;

impl InviteCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    async fn text_invite<'a>(&self, pld: &CommandPayload, url: String) -> anyhow::Result<()> {
        let embed = CreateEmbed::default()
            .color(0x1b_6f_5f)
            .description(format!("Click the following link to invite me: <{}>", url));
        let msg = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, msg)
            .await?;
        Ok(())
    }

    async fn link_invite<'a>(
        &self,
        pld: &CommandPayload,
        bot: CurrentUser,
        url: String,
    ) -> anyhow::Result<()> {
        let avatar_url = Avatar::url(User::from(bot.clone()));
        let color = ImageProcessing::from_url(avatar_url.clone()).await;
        let author = CreateEmbedAuthor::new(format!("{}: Project Unicorn", bot.name))
            .icon_url(avatar_url.clone())
            .url(url);
        let embed = CreateEmbed::default().color(color).author(author);
        let msg = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, msg)
            .await?;
        Ok(())
    }
}

#[serenity::async_trait]
impl UnicornCommand for InviteCommand {
    fn command_name(&self) -> &str {
        "invite"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["inv"]
    }

    fn description(&self) -> &str {
        "Provides Sigma's invitation link to add her to your server. \
         If you can't click/tap embed title URLs, add the word \"text\" \
         to the command to get the invite in a plain text format."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("text", true).flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let bot = pld.ctx.cache.current_user().clone();
        let invite_url = format!(
            "https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8",
            bot.id
        );
        if pld.args.has_argument("text") {
            self.text_invite(pld, invite_url).await?;
        } else {
            self.link_invite(pld, bot, invite_url).await?;
        };
        Ok(())
    }
}
