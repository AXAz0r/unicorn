use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

const LICENSE_URL: &str = "https://gitlab.com/lu-ci/unicorn/-/blob/master/LICENSE.md";
const PROJECT_ICON: &str =
    "https://gitlab.com/uploads/-/system/project/avatar/12884739/project_unicorn_pico.png";

const LICENSE_SHORT: &str = "
Copyright (C) 2021  Lucia's Cipher

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
";

#[derive(Default)]
pub struct LicenseCommand;

impl LicenseCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for LicenseCommand {
    fn command_name(&self) -> &str {
        "license"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn description(&self) -> &str {
        "Shows the license of the project."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let author = CreateEmbedAuthor::new("Project Unicorn License")
            .url(LICENSE_URL)
            .icon_url(PROJECT_ICON);
        let embed = CreateEmbed::default()
            .author(author)
            .description(LICENSE_SHORT);
        let message = CreateMessage::default().embed(embed);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, message)
            .await?;
        Ok(())
    }
}
