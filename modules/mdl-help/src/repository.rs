use serenity::builder::{CreateEmbed, CreateEmbedAuthor, CreateMessage};

use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct RepositoryCommand;

impl RepositoryCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn text_invite(&self, url: String) -> CreateMessage {
        let embed = CreateEmbed::default()
            .color(0x1b_6f_5f)
            .description(format!("Apex Sigma: The Database Giant: <{}>", url));
        CreateMessage::default().embed(embed)
    }

    fn link_invite(&self, url: String) -> CreateMessage {
        let author = CreateEmbedAuthor::new("Apex Sigma: The Database Giant")
            .icon_url("https://framablog.org/wp-content/uploads/2016/01/gitlab.png")
            .url(url);
        let embed = CreateEmbed::default().author(author);
        CreateMessage::default().embed(embed)
    }
}

#[serenity::async_trait]
impl UnicornCommand for RepositoryCommand {
    fn command_name(&self) -> &str {
        "repository"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["repo", "project"]
    }

    fn description(&self) -> &str {
        "Shows the link to the project's repository page. \
         You can add \"text\" to the command to show just the link as pure text."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("text", true).flag()]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let invite_url = "https://gitlab.com/lu-ci/sigma/apex-sigma".to_string();
        let response = if pld.args.has_argument("text") {
            self.text_invite(invite_url)
        } else {
            self.link_invite(invite_url)
        };

        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
