use serenity::builder::{CreateEmbed, CreateMessage};

use unicorn_callable::command::UnicornCommand;
use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct DonateCommand;

impl DonateCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    fn donate_information(&self) -> CreateMessage {
        let donation_url = "https://www.patreon.com/ApexSigma";
        let embed = CreateEmbed::default()
            .color(0x1b_6f_5f)
            .title("Sigma Donation Information")
            .description(format!(
                "Care to help out? Come [support]({}) Sigma!",
                donation_url
            ));
        CreateMessage::default().embed(embed)
    }
}

#[serenity::async_trait]
impl UnicornCommand for DonateCommand {
    fn command_name(&self) -> &str {
        "donate"
    }

    fn category(&self) -> &str {
        "help"
    }

    fn description(&self) -> &str {
        "Shows donation information for Sigma."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = self.donate_information();
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
