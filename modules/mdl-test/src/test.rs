use serenity::builder::CreateMessage;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct TestCommand;

impl TestCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for TestCommand {
    fn command_name(&self) -> &str {
        "test"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn description(&self) -> &str {
        "Just tests some random function."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = CreateMessage::default().content("Tasty tests tested tastefuly.");
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
