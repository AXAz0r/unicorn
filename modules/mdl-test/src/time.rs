use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;

use crate::timezone::TimezoneConfig;

pub struct TimeCommand;

impl TimeCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for TimeCommand {
    fn command_name(&self) -> &str {
        "time"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn description(&self) -> &str {
        "Displays the current time in UTC."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let time = chrono::Utc::now();
        let mut msg = format!("Time is {} {}", time.format("%d. %m. %Y - %H:%M:%S"), "UTC");
        if let Some(timezone_cfg) = pld.cfg.get_custom::<TimezoneConfig>() {
            if let Some(offset) = chrono::offset::FixedOffset::west_opt(timezone_cfg.offset) {
                let offset_time = time.with_timezone(&offset);
                msg = format!(
                    "Time is {} {}",
                    offset_time.format("%d. %m. %Y - %H:%M:%S"),
                    timezone_cfg.timezone
                );
            }
        }
        let embed = CreateEmbed::default().color(16_382_457).title(format!("🕥 {}", msg));
        let message = CreateMessage::default().embed(embed);
        let _ = pld
            .msg
            .channel_id
            .send_message(&pld.ctx.http, message)
            .await;
        Ok(())
    }
}
