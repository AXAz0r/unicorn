use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_cache::handler::CacheHandler;

use unicorn_callable::payload::CommandPayload;
use unicorn_callable::UnicornCommand;
use unicorn_database::DatabaseHandler;
use unicorn_utility::image::ImageProcessing;

pub struct AvatarCommand;

impl AvatarCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }

    async fn save_color_to_cache(&self, cache: CacheHandler, url: String, color: u64) {
        cache.set(url, color.to_string()).await;
    }

    async fn find_color_in_cache(&self, cache: CacheHandler, url: String) -> Option<u64> {
        match cache.get(url).await {
            Some(val) => match val.parse::<u64>() {
                Ok(val) => Some(val),
                Err(_) => None,
            },
            None => None,
        }
    }

    async fn save_color_to_db(&self, _db: DatabaseHandler, _url: String, _color: u64) {}

    async fn find_color_in_db(&self, _db: DatabaseHandler, _url: String) -> Option<u64> {
        None
    }

    async fn save_color(&self, db: DatabaseHandler, cache: CacheHandler, url: String, color: u64) {
        self.save_color_to_db(db, url.clone(), color).await;
        self.save_color_to_cache(cache, url, color).await;
    }

    async fn find_color(
        &self,
        db: DatabaseHandler,
        cache: CacheHandler,
        url: String,
    ) -> Option<u64> {
        match self.find_color_in_cache(cache.clone(), url.clone()).await {
            Some(val) => Some(val),
            None => match self.find_color_in_db(db, url.clone()).await {
                Some(val) => {
                    self.save_color_to_cache(cache, url, val).await;
                    Some(val)
                }
                None => None,
            },
        }
    }
}

#[serenity::async_trait]
impl UnicornCommand for AvatarCommand {
    fn command_name(&self) -> &str {
        "avatar"
    }

    fn category(&self) -> &str {
        "utility"
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["av", "pfp"]
    }

    fn description(&self) -> &str {
        "Displays the mentioned user's avatar, or the author's if no use is mentioned."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let target = if !pld.msg.mentions.is_empty() {
            pld.msg.mentions[0].clone()
        } else {
            pld.msg.author.clone()
        };
        let avatar = match target.avatar_url() {
            Some(av) => av,
            None => target.default_avatar_url(),
        };
        let color = match self
            .find_color(pld.db.clone(), pld.cache.clone(), avatar.clone())
            .await
        {
            Some(clr) => clr,
            None => {
                let clr = ImageProcessing::from_url(avatar.clone()).await;
                self.save_color(pld.db.clone(), pld.cache.clone(), avatar.clone(), clr)
                    .await;
                clr
            }
        };
        let embed = CreateEmbed::default().color(color).image(avatar);
        let msg = CreateMessage::default().embed(embed);
        let _ = pld.msg.channel_id.send_message(&pld.ctx.http, msg).await;
        Ok(())
    }
}
