use serenity::builder::CreateMessage;
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;

#[derive(Default)]
pub struct SendCommand;

impl SendCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for SendCommand {
    fn command_name(&self) -> &str {
        "send"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn example(&self) -> &str {
        "u:0123456789 We are watching..."
    }

    fn description(&self) -> &str {
        "Sends a message to a user, channel or server. \
         The first argument needs to be the destination parameter. \
         The destination parameter consists of the destination type and ID. \
         The types are U for User and C for Channel. \
         The type and ID are separated by a colon, or two dots put more simply."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![
            CommandArgument::new("controller", false),
            CommandArgument::new("content", false).allows_spaces(),
        ]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if pld.args.satisfied() {
            let controller = pld.args.get("controller");
            let content = pld.args.get("content");
            let m = CreateMessage::default().content(content);
            let control_pieces: Vec<&str> = controller.split(':').collect();
            if control_pieces.len() == 2 {
                let mode = control_pieces[0];
                let id_str = control_pieces[1];
                if let Ok(id) = id_str.parse::<u64>() {
                    match mode.to_lowercase().as_ref() {
                        "u" => {
                            let user = pld.ctx.cache.user(id).map(|user| user.clone());
                            if let Some(user) = user {
                                let dm_ok = user
                                    .dm(&pld.ctx.http, m)
                                    .await
                                    .is_ok();
                                if dm_ok {
                                    UnicornEmbed::ok(format!(
                                        "Message sent to {}#{:0>4}.",
                                        &user.name,
                                        if let Some(disc) = user.discriminator {
                                            disc.get()
                                        } else {
                                            0
                                        }
                                    ))
                                } else {
                                    UnicornEmbed::error(format!(
                                        "Failed to message {}#{:0>4}.",
                                        &user.name,
                                        if let Some(disc) = user.discriminator {
                                            disc.get()
                                        } else {
                                            0
                                        },
                                    ))
                                }
                            } else {
                                UnicornEmbed::not_found("User not found.")
                            }
                        }
                        "c" => {
                            let mut tgt_channel = None;
                            for guild in pld.ctx.cache.guilds() {
                                if let Ok(channels) = guild.channels(&pld.ctx.http).await {
                                    for (channel_id, channel) in channels {
                                        if channel_id.get() == id {
                                            tgt_channel = Some(channel);
                                            break;
                                        }
                                    }
                                }
                                if tgt_channel.is_some() {
                                    break;
                                }
                            }
                            if let Some(channel) = tgt_channel {
                                let channel_name = channel
                                    .name();
                                let send_ok = channel
                                    .send_message(&pld.ctx.http, m)
                                    .await
                                    .is_ok();
                                if send_ok {
                                    UnicornEmbed::ok(format!("Message sent to #{}.", channel_name))
                                } else {
                                    UnicornEmbed::error(format!(
                                        "Failed to message #{}.",
                                        channel_name
                                    ))
                                }
                            } else {
                                UnicornEmbed::not_found("Channel not found.")
                            }
                        }
                        _ => UnicornEmbed::error("The target type is invalid."),
                    }
                } else {
                    UnicornEmbed::error("The target ID is invalid.")
                }
            } else {
                UnicornEmbed::error("The controller argument is invalid.")
            }
        } else {
            UnicornEmbed::error("Not enough arguments, check the command example.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
