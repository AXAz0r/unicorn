use std::borrow::Cow;

use serenity::builder::{CreateAttachment, CreateMessage};
use serenity::model::guild::Guild;

use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct GuildDumpCommand;

impl GuildDumpCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

impl GuildDumpCommand {
    fn guild_to_row(gld: &Guild) -> Vec<String> {
        let cells = vec![gld.id.get().to_string(), gld.name.clone()];
        cells
    }

    fn rows_to_csv(rows: Vec<Vec<String>>) -> String {
        let mut rtxs = Vec::<String>::new();
        for row in rows {
            let rtx = row.join("\t");
            rtxs.push(rtx);
        }
        rtxs.join("\n")
    }
}

#[serenity::async_trait]
impl UnicornCommand for GuildDumpCommand {
    fn command_name(&self) -> &str {
        "allguilds"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn aliases(&self) -> Vec<&str> {
        vec!["allservers"]
    }

    fn description(&self) -> &str {
        "Dumps the information of all served guilds into a CSV file upload."
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let mut rows = Vec::<Vec<String>>::new();
        let mut pcount = 0;
        for gid in pld.ctx.cache.guilds() {
            if let Some(guild) = pld.ctx.cache.guild(gid) {
                let row = Self::guild_to_row(&guild);
                rows.push(row);
                pcount += 1;
            }
        }
        let csv = Self::rows_to_csv(rows);
        let bytes = csv.as_bytes();
        let attachment = CreateAttachment::bytes(
            Cow::from(bytes),
            format!("server_list_{}.csv", pld.msg.id.get()),
        );
        let response = CreateMessage::default()
            .content(format!(
                "✅ Dumped information on {} served guilds.",
                pcount
            ))
            .add_file(attachment);
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
