use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;

#[derive(Default)]
pub struct ShutDownCommand;

impl ShutDownCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for ShutDownCommand {
    fn command_name(&self) -> &str {
        "shutdown"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn description(&self) -> &str {
        "Forces the bot to shut down in a dirty manner.\
         Connections will not be closed and the instance will just be killed."
    }

    //noinspection RsUnreachableCode
    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let _ = pld.msg.react(&pld.ctx.http, '💀').await;
        std::process::exit(0);
        #[allow(unreachable_code)]
        Ok(())
    }
}
