use serenity::builder::{CreateEmbed, CreateMessage};
use unicorn_callable::argument::CommandArgument;
use unicorn_callable::command::UnicornCommand;

use unicorn_callable::payload::CommandPayload;
use unicorn_embed::UnicornEmbed;
use unicorn_error::UnicornError;

#[derive(Default)]
pub struct GetErrorCommand;

impl GetErrorCommand {
    pub fn boxed() -> Box<dyn UnicornCommand> {
        Box::new(Self)
    }
}

#[serenity::async_trait]
impl UnicornCommand for GetErrorCommand {
    fn command_name(&self) -> &str {
        "geterror"
    }

    fn category(&self) -> &str {
        "development"
    }

    fn owner(&self) -> bool {
        true
    }

    fn example(&self) -> &str {
        "9a2e9a374ac90294f225782f362e2ab1"
    }

    fn description(&self) -> &str {
        "Get the details of the error with the specified token."
    }

    fn parameters(&self) -> Vec<CommandArgument> {
        vec![CommandArgument::new("token", false)]
    }

    async fn execute(&self, pld: &mut CommandPayload) -> anyhow::Result<()> {
        let response = if !pld.args.is_empty() {
            let token = pld.args.get("token").to_string();
            if let Some(error) = UnicornError::get(&pld.db, token).await {
                let ecmd = format!(
                    "Command: **{}**\nCategory: **{}**\nMessage: **{}**\nArguments: **{}**",
                    &error.message.cmd.name,
                    &error.message.cmd.category,
                    &error.message.id,
                    if error.message.cmd.args.is_empty() {
                        "<no arguments>".to_owned()
                    } else {
                        error.message.cmd.args.to_string()
                    }
                );
                let eorg = format!(
                    "Author: **{}**\n- {}\nChannel: **#{}**\n- {}\nGuild: **{}**\n- {}",
                    error.user.name,
                    error.user.id,
                    error.channel.name,
                    error.channel.id,
                    error.guild.name,
                    error.guild.id
                );
                let etrc = format!("```hs\n{}\n```", &error.error.trace);
                let embed = CreateEmbed::default()
                    .color(0xbe_19_31)
                    .title(format!("❗ Error: {}", &error.token))
                    .field("Call", ecmd, true)
                    .field("Origin", eorg, true)
                    .field("Details", etrc, false);
                CreateMessage::default().embed(embed)
            } else {
                UnicornEmbed::not_found("No error with that token was found.")
            }
        } else {
            UnicornEmbed::error("No error token given.")
        };
        pld.msg
            .channel_id
            .send_message(&pld.ctx.http, response)
            .await?;
        Ok(())
    }
}
